package com.example.giovanni.primoesercizioquarta;

/**
 * Created by giovanni on 04/03/2016.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class FirstScreen extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.firstscreen);

        try {
            wait(5000);
        } catch (InterruptedException e) {

        }

        Intent i=new Intent(getBaseContext(),MainActivity.class);
        startActivity(i);
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();

    }
}