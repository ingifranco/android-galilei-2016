package com.example.giovanni.primoesercizioquarta;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText e_a, e_b, e_c;
    TextView tv_x1, tv_x2;
    Button btCalc, btClear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        e_a = (EditText)findViewById(R.id.ed_coeff_a);
        e_b = (EditText)findViewById(R.id.ed_coeff_b);
        e_c = (EditText)findViewById(R.id.ed_coeff_c);

        tv_x1 = (TextView)findViewById(R.id.textX1);
        tv_x2 = (TextView)findViewById(R.id.textX2);

        btCalc = (Button)findViewById(R.id.btCalcola);
        btClear = (Button)findViewById(R.id.btClear);

        btCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    double a = Double.parseDouble(e_a.getText().toString());
                    double b = Double.parseDouble(e_b.getText().toString());
                    double c = Double.parseDouble(e_c.getText().toString());
                    double d = b * b - 4.0 * a * c;

                    tv_x1.setText("");
                    tv_x2.setText("");

                    if (d >= 0.0) {
                        double x1 = (-b + Math.sqrt(d)) / (2.0 * a);
                        double x2 = (-b - Math.sqrt(d)) / (2.0 * a);
                        String s_x1 = String.format("%.5f", x1);
                        String s_x2 = String.format("%.5f", x2);

                        tv_x1.setText(s_x1);
                        tv_x2.setText(s_x2);
                    }
                    else{
                        throw new Exception("ATTENZIONE: determinante negativo");
                    }
                }
                catch(Exception e){
                    ClearAll();

                    Toast.makeText(
                            getApplicationContext(),
                            e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
            }
        });

        btClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClearAll();
            }
        });
    }

    private void ClearAll(){
        tv_x1.setText("");
        tv_x2.setText("");
        e_b.setText("");
        e_c.setText("");
        e_a.setText("");
        e_a.requestFocus();
    }
}
