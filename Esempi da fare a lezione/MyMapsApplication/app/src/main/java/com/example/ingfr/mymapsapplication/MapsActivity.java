package com.example.ingfr.mymapsapplication;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);


        // Add a marker in Sydney and move the camera
        LatLng ss1 = new LatLng(44.915737, 10.230111);
        mMap.addMarker(new MarkerOptions().position(ss1).title("Marker in S9ydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ss1, 18));
        //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(focus, Integer.parseInt(getString(R.string.default_zoom))));

        LatLng ss2 = new LatLng(44.916536, 10.230264);
        mMap.addMarker(new MarkerOptions().position(ss2).title("Marker in Sydney"));
        //<mMap.moveCamera(CameraUpdateFactory.newLatLng(ss1));
    }
}
