package com.example.giovanni.myapplication;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText username;
    EditText password;
    TextView infos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        username = (EditText)findViewById(R.id.editText);
        password = (EditText)findViewById(R.id.editText2 );
        infos = (TextView)findViewById(R.id.textView3);
    }
    //Save infos
    public void saveInfo(View v){
        SharedPreferences sharePref = getSharedPreferences("userInfo", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharePref.edit();
        editor.putString("username", username.getText().toString());
        editor.putString("password", password.getText().toString());
        editor.apply();

        Toast.makeText(this, "saved preferences", Toast.LENGTH_LONG).show();
    }

    // print out infos
    public void displayData(View v){
        SharedPreferences sharePref = getSharedPreferences("userInfo", Context.MODE_PRIVATE);

        String name = sharePref.getString("username", "");
        String pass = sharePref.getString("password", "");

        infos.setText(name + " " + pass);
    }
}
