package com.example.giovanni.calcolatrice2016;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    //final static int
    Double op1, op2;
    String strOpSel;
    int status = 0;
    String strtemp;
    EditText edResult;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edResult = (EditText)findViewById(R.id.result_id);
    }

    private void addChar(String c){
        switch(status){
            case 0:
                strtemp = edResult.getText().toString();
                strtemp += c;
                edResult.setText(strtemp);
                break;
            case 1:
                edResult.setText(c);
                status = 2;
                break;
            case 2:
                break;
        }

    }


    private void setOp(String o){
        strOpSel = o;
        switch(status){
            case 0:
                op1 = Double.parseDouble(edResult.getText().toString());
                status = 1;
                break;

            case 1:
                op2 = Double.parseDouble(edResult.getText().toString());
                if(o.equals("=")){
                    run();
                }
                status = 2;
                break;

            case 2:
                break;

        }
    }



    private void run(){
        Double result;

        if(strOpSel.equals("+")){
            result = op1 + op2;
        }
        else
        if(strOpSel.equals("-")){
            result = op1 - op2;
        }
        else
        if(strOpSel.equals("//")){
            if(op2 != 0.0){
                result = op1 / op2;
            }
            else{

            }

        }
        else
        if(strOpSel.equals("*")){
            result = op1 + op2;
        }


    }

    public void onClick(View v){
        int id = v.getId();

        switch(id){
            case R.id.Btn0_id:
                addChar("0");
                break;
            case R.id.Btn1_id:
                addChar("1");
                break;
            case R.id.Btn2_id:
                addChar("2");
                break;
            case R.id.Btn3_id:
                addChar("3");
                break;
            case R.id.Btn4_id:
                addChar("4");
                break;
            case R.id.Btn5_id:
                addChar("5");
                break;
            case R.id.Btn6_id:
                addChar("6");
                break;
            case R.id.Btn7_id:
                addChar("7");
                break;
            case R.id.Btn8_id:
                addChar("8");
                break;
            case R.id.Btn9_id:
                addChar("9");
                break;
            case R.id.Btnequal_id:
                run();
                break;
            case R.id.Btnclear_id:
                edResult.setText("");
                break;
            case R.id.Btndivide_id:
                setOp("//");
                break;
            case R.id.Btnminus_id:
                setOp("-");
                break;
            case R.id.Btnmulti_id:
                setOp("*");
                break;
            case R.id.Btnplus_id:
                setOp("+");
                break;
            case R.id.Btndot_id:
                addChar(".");
                break;
        }
    }
}
