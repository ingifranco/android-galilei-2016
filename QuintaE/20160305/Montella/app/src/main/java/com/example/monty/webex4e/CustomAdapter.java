package com.example.monty.webex4e;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class CustomAdapter extends ArrayAdapter<ListData> {

	Context mContext;
	ListData[] data;
	int layoutResourceId;

	public CustomAdapter(Context mContext, int layoutResourceId, ListData[] data) {
		super(mContext, layoutResourceId, data);

		this.layoutResourceId = layoutResourceId;
		this.mContext = mContext;
		this.data = data;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			// inflate the layout
			LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
			convertView = inflater.inflate(layoutResourceId, parent, false);
		}

		final ListData objectItem = data[position];

		ImageView image = (ImageView) convertView.findViewById(R.id.imageView1);
		image.setImageResource(objectItem.getIcon());

		TextView textViewItem1 = (TextView) convertView.findViewById(R.id.textView1);
		textViewItem1.setText(objectItem.getName());

		TextView textViewItem2 = (TextView) convertView.findViewById(R.id.textView2);
		textViewItem2.setText(objectItem.getDesc());

		convertView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String msg = "Hai cliccato su " + data[position].getName() + "(" + position + ")";
				Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();

				Intent intent = new Intent(mContext, EditActivity.class);
				intent.putExtra("Icon", Integer.toString(objectItem.getIcon()));
				intent.putExtra("Name", objectItem.getName());
				intent.putExtra("Desc", objectItem.getDesc());

				mContext.startActivity(intent);
			}
		});

		return convertView;

	}
}