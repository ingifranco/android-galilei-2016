package com.example.user.unoverifica;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class MainActivity extends AppCompatActivity {
    WebView myWebView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myWebView = (WebView) findViewById(R.id.mia_web_view);
        // abilitiamo JavScript
        WebSettings webs = myWebView.getSettings();
        webs.setJavaScriptEnabled(true);
        myWebView.loadUrl("SITO.SITO");
    }
}
