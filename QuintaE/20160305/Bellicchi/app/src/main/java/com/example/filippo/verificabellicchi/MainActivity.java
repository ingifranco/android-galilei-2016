package com.example.filippo.verificabellicchi;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.widget.ListView;

public class MainActivity extends ActionBarActivity {


    ListView lv;
    Context context;
    liste[] ObjectItemData = new liste[3];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int i = 0;

        ObjectItemData[i++] = new liste("W3C", "http//www.w3school.com/", R.drawable.android_browser_256);
        ObjectItemData[i++] = new liste("GOOGLE", "http//google.it/", R.drawable.android_bluetooth_256);
        ObjectItemData[i++] = new liste("METEO", "http//www.openweathermap.com/", R.drawable.openweathermap_orange_website);

        context = this;
        java adapter = new java(context, R.layout.elementi, ObjectItemData);
        lv = (ListView) findViewById(R.id.listView);
        lv.setAdapter(adapter);

    }


}

