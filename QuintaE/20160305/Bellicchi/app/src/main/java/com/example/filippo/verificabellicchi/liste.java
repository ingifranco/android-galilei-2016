package com.example.filippo.verificabellicchi;

/**
 * Created by Filippo on 07/03/2016.
 */
public class liste {

    private String name;
    private String desc;
    private int icon;

    public liste(String name, String desc, int icon) {
        this.setName(name);
        this.setDesc(desc);
        this.setIcon(icon);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

}
