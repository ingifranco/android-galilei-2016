package com.example.filippo.verificabellicchi;

        import android.support.v7.app.ActionBarActivity;
        import android.util.Log;
        import android.os.Bundle;
        import android.view.Menu;
        import android.view.MenuItem;
        import android.view.View;
        import android.webkit.WebSettings;
        import android.webkit.WebView;
        import android.widget.ImageView;
        import android.widget.TextView;
        import android.widget.Toast;

@SuppressWarnings("deprecation")
public class EditActivity extends ActionBarActivity {

    private String name;
    private String desc;
    private String icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.elementi);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            icon = extras.getString("Icon");
            name = extras.getString("Name");
            desc = extras.getString("Desc");
WebView myWebView;
            myWebView = (WebView) findViewById(R.id.mia_web_view);
            //abilitiamo javaScript

            WebSettings webs = myWebView.getSettings();
            webs.setJavaScriptEnabled(true);

            //mi collego al sito qua sotto
            myWebView.loadUrl (desc);


        }
    }
}