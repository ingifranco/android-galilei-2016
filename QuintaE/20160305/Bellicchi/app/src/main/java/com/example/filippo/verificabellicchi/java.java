package com.example.filippo.verificabellicchi;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Filippo on 07/03/2016.
 */
public class java extends ArrayAdapter<liste> {

    Context mContext;
    liste[] data;
    int layoutResourceId;

    public java(Context mContext, int layoutResourceId, liste[] data) {
        super(mContext, layoutResourceId, data);

        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            // inflate the layout
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);
        }

        final liste objectItem = data[position];

        ImageView image = (ImageView) convertView.findViewById(R.id.imageView1);
        image.setImageResource(objectItem.getIcon());

        TextView textViewItem1 = (TextView) convertView.findViewById(R.id.textView1);
        textViewItem1.setText(objectItem.getName());

        TextView textViewItem2 = (TextView) convertView.findViewById(R.id.textView2);
        textViewItem2.setText(objectItem.getDesc());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Intent intent = new Intent(mContext, liste.class);
                intent.putExtra("Icon", Integer.toString(objectItem.getIcon()));
                intent.putExtra("Name", objectItem.getName());
                intent.putExtra("Desc", objectItem.getDesc());

                mContext.startActivity(intent);
            }
        });

        return convertView;

    }
}
