package com.example.user.verificanumerouno;

public class ListData {
	private String name;
	private String Link;
	private int icon;

	public ListData(String name, String desc, int icon) {
		this.setName(name);
		this.setLink(Link);
		this.setIcon(icon);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIcon() {
		return icon;
	}

	public void setIcon(int icon) {
		this.icon = icon;
	}

	public String getLink() {
		return Link;
	}

	public void setLink(String link) {
		this.Link = Link;
	}



}
