package com.example.user.verificanumerouno;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ListView;

@SuppressWarnings("deprecation")
public class MainActivity extends ActionBarActivity {

	ListView lv;
	Context context;
	ListData[] ObjectItemData = new ListData[9];
	WebView myWebView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		myWebView = (WebView)findViewById(R.id.mia_web_view);
		//Abilitiamo Javascript
		WebSettings webs = myWebView.getSettings();
		webs.setJavaScriptEnabled(true);

		int i = 0;

		ObjectItemData[i++] = new ListData("W3C", "https://www,w3school.com/", R.drawable.android_browser_256);
		ObjectItemData[i++] = new ListData("Google", "https://www.google.it/", R.drawable.android_bluetooth_256);
		ObjectItemData[i++] = new ListData("Meteo", "https://www.openweathermap.org/", R.drawable.openweathermap_orange_website);

		context = this;
		CustomAdapter adapter = new CustomAdapter(context, R.layout.program_list, ObjectItemData);
		lv = (ListView) findViewById(R.id.listView);
		lv.setAdapter(adapter);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
