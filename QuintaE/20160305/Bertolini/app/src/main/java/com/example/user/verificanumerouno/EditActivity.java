package com.example.user.verificanumerouno;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.webkit.WebSettings;
import android.webkit.WebView;

import android.widget.Toast;

@SuppressWarnings("deprecation")
public class EditActivity extends ActionBarActivity {

	private String name;
	private String desc;
	private String icon;
	private String link;
	WebView myWebView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_list);

		Bundle extras = getIntent().getExtras();

		if (extras != null) {
			WebSettings web = myWebView.getSettings();
			icon = extras.getString("Icon");
			name = extras.getString("Name");
			link = extras.getString("Link");
			web.setJavaScriptEnabled(true);

			if(name == "W3C")
			{
				myWebView.loadUrl(link);
			}
			if(name == "Google")
			{
				myWebView.loadUrl(link);
			}
			if(name == "Meteo")
			{
				myWebView.loadUrl(link);
			}
		}
	}
	
	public void onClick(View v) {

		final int id = v.getId();

		switch (id) {
		case R.id.button1:
			Toast.makeText(getApplicationContext(), "Bye!!!!", Toast.LENGTH_LONG).show();
			break;
		case R.id.imageView1:
			Toast.makeText(getApplicationContext(), "RIIIIBye!!!!", Toast.LENGTH_LONG).show();
			break;
		default:
			// your code for button2 here
			break;
		// even more buttons here
		}

		
		finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
