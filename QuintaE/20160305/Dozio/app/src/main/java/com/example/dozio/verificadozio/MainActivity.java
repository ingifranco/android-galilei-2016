package com.example.dozio.verificadozio;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    ListView myListView;
    Context context;
    ListData[] ObjectItemData = new ListData[3];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int i = 0;

        ObjectItemData[i++] = new ListData("W3 Schools", "http://www.w3schools.com/", R.drawable.android_browser_256);
        ObjectItemData[i++] = new ListData("Google", "http://www.google.com/",R.drawable.android_bluetooth_256);
        ObjectItemData[i++] = new ListData("Meteo", "http://www.ilmeteo.it",R.drawable.openweathermap_orange_website);

        context = this;
        CustomAdapter adapter = new CustomAdapter(context, R.layout.program_list, ObjectItemData);
        myListView = (ListView)findViewById(R.id.listView);

        myListView.setAdapter(adapter);
    }
}
