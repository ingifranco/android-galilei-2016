package com.example.dozio.verificadozio;

/**
 * Created by dozio on 3/8/2016.
 */
public class ListData {
    private String name;
    private String adress;
    private int icon;

    public ListData(String name, String adress, int icon)
    {
        setName(name);
        setAdress(adress);
        setIcon(icon);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
