package com.example.dozio.verificadozio;;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dozio.verificadozio.R;

@SuppressWarnings("deprecation")
public class EditActivity extends ActionBarActivity {

    private String name;
    private String adress;
    private String icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_list);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            adress = extras.getString("Adress");

            WebView wv = (WebView) findViewById(R.id.webView);
            WebSettings ws = wv.getSettings();
            ws.setJavaScriptEnabled(true);
            wv.loadUrl(adress);
            ImageButton ImgButton = (ImageButton)findViewById(R.id.imageButton);
            ImgButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {

                    finish();
                }
            });

        }
    }
}