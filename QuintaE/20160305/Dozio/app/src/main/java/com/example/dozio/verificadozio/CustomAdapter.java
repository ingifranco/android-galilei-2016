package com.example.dozio.verificadozio;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by dozio on 3/8/2016.
 */
public class CustomAdapter extends ArrayAdapter<ListData>{

    Context context;
    ListData[] data;
    int resource;

    public CustomAdapter(Context context, int resource, ListData[] data) {
        super(context, resource, data);

        this.resource = resource;
        this.context = context;
        this.data = data;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        if(convertView == null) {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            convertView = inflater.inflate(resource, parent, false);
        }

        final ListData objectItem = data[position];

        ImageView image = (ImageView) convertView.findViewById(R.id.imageView1);
        image.setImageResource(objectItem.getIcon());

        TextView textViewItem1 = (TextView) convertView.findViewById(R.id.textView1);
        textViewItem1.setText(objectItem.getName());

        TextView textViewItem2 = (TextView) convertView.findViewById(R.id.textView2);
        textViewItem2.setText(objectItem.getAdress());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Intent intent = new Intent(context, EditActivity.class);
                intent.putExtra("Icon", Integer.toString(objectItem.getIcon()));
                intent.putExtra("Adress", objectItem.getAdress());
                intent.putExtra("Name", objectItem.getName());


                context.startActivity(intent);
            }
        });

        return convertView;
    }
}
