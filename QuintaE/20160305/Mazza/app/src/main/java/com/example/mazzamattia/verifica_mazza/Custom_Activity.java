package com.example.mazzamattia.verifica_mazza;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by MazzaMattia on 07/03/16.
 */
public class Custom_Activity extends ArrayAdapter<Lista_Activity> {

    Context context2;
    Lista_Activity[] data;
    int layoutResourceId;

    public Custom_Activity(Context context2, int layoutResourceId, Lista_Activity[] data) {
        super(context2, layoutResourceId, data);

        this.layoutResourceId = layoutResourceId;
        this.context2 = context2;
        this.data = data;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            // inflate the layout
            LayoutInflater inflater = ((Activity) context2).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);
        }

        final Lista_Activity objectItem = data[position];

        ImageView image = (ImageView) convertView.findViewById(R.id.imageView1);
        image.setImageResource(objectItem.getIcona());

        TextView textViewItem1 = (TextView) convertView.findViewById(R.id.textView1);
        textViewItem1.setText(objectItem.getNome());


        TextView textViewItem2 = (TextView) convertView.findViewById(R.id.textView2);
        textViewItem2.setText(objectItem.getDescrizione());




        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Intent intent = new Intent(context2, Edit_Activity.class);


                intent.putExtra("Icona", Integer.toString(objectItem.getIcona()));
                intent.putExtra("Descrizione", objectItem.getDescrizione());
                intent.putExtra("Nome", objectItem.getNome());


                context2.startActivity(intent);
            }
        });

        return convertView;

    }
}