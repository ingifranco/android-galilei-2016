package com.example.mazzamattia.verifica_mazza;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    ListView lista;
    Context context;
    Lista_Activity[] ObjectItemData = new Lista_Activity[3];

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int num = 0;
        ObjectItemData[num++] = new Lista_Activity("Meteo", "http://openweathermap.org/", R.drawable.openweathermap_orange_website);
        ObjectItemData[num++] = new Lista_Activity("W3C", "http://www.w3schools.com/", R.drawable.android_browser_256);
        ObjectItemData[num++] = new Lista_Activity("Google", "https://www.google.it/", R.drawable.android_bluetooth_256);


        context = this;
        Custom_Activity adapter = new Custom_Activity(context, R.layout.program_layout, ObjectItemData);


            lista = (ListView)findViewById(R.id.ListView1);
            lista.setAdapter(adapter);

    }
}
