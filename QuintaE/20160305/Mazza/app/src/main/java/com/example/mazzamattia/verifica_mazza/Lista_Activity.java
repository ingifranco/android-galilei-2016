package com.example.mazzamattia.verifica_mazza;

/**
 * Created by MazzaMattia on 07/03/16.
 */
public class Lista_Activity {
        private String nome;
        private String descrizione;
        private int icona;

        public Lista_Activity(String descrizione, String nome, int icona) {

            this.setNome(nome);

            this.setDescrizione(descrizione);

            this.setIcona(icona);
        }

        public String getNome() {
            return nome;
        }
          public void setNome(String nome) {
            this.nome = nome;
        }

            public String getDescrizione() {
            return descrizione;
        }
              public void setDescrizione(String descrizione) {
            this.descrizione = descrizione;
        }

                public int getIcona() {
            return icona;
        }
                  public void setIcona(int icona) {
            this.icona = icona;
        }


}
