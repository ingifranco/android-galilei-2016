package com.example.mazzamattia.verifica_mazza;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageButton;

/**
 * Created by MazzaMattia on 07/03/16.
 */
public class Edit_Activity extends AppCompatActivity {


    private String descrizione;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_layout);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            descrizione = extras.getString("Descrizione");

            WebView myWebView = (WebView) findViewById(R.id.webView);
               WebSettings webs = myWebView.getSettings();
                 webs.setJavaScriptEnabled(true);
                    myWebView.loadUrl(descrizione);


            ImageButton ImgButton = (ImageButton)findViewById(R.id.imageButton);
            ImgButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {

                    finish();

//
                }
            });

        }
    }



}
