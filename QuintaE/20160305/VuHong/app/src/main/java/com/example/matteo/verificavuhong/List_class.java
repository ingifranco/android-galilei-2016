package com.example.matteo.verificavuhong;
/**
 * Created by Matteo on 05/03/2016.
 */
public class List_class {
    int IDicona;
    String Nome, Link;

    public List_class(int IDicona, String nome, String link) {
        this.IDicona = IDicona;
        Nome = nome;
        Link = link;
    }

    public int getIDicona() {
        return IDicona;
    }

    public void setIDicona(int IDicona) {
        this.IDicona = IDicona;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    public String getLink() {
        return Link;
    }

    public void setLink(String link) {
        Link = link;
    }
}
