package com.example.matteo.verificavuhong;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<List_class> miaLista = new ArrayList<List_class>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        riempilista(); //richiamo funzione
        riempiListView();
        gestioneclick();

    }

    private void gestioneclick() {
        ListView lista = (ListView)findViewById(R.id.listView);
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                List_class OggCliccato = miaLista.get(position);
                String mss = "Hai cliccato " + OggCliccato.getNome();
                Toast.makeText(MainActivity.this, mss, Toast.LENGTH_SHORT).show();

               Intent intent = new Intent(MainActivity.this, OpenWeb_class.class);
                intent.putExtra("Il nostro valore", OggCliccato.getLink());
                startActivity(intent);
            }
        });



    }

    private void riempiListView() {
        ArrayAdapter<List_class> adapter = new MioAdapter();
        ListView lista = (ListView)findViewById(R.id.listView);
        lista.setAdapter(adapter);
    }

    private void riempilista() {
        miaLista.add(new List_class(R.drawable.android_browser_256, "W3C", "http://www.w3schols.com/"));
        miaLista.add(new List_class(R.drawable.android_bluetooth_256, "Google", "http://www.google.it/"));
        miaLista.add(new List_class(R.drawable.openweathermap_orange_website, "Meteo", "http://openweathermap.org/"));

    }

    private class MioAdapter extends ArrayAdapter<List_class>{

        public MioAdapter(){
            super(MainActivity.this, R.layout.lista1, miaLista);
        }
        public View getView(int position, View convertView, ViewGroup parent){
            View oggetto = convertView;
            if (oggetto == null){
                oggetto = getLayoutInflater().inflate(R.layout.lista1, parent, false);
            }
            List_class nome_corrente = miaLista.get(position);

            ImageView img = (ImageView)oggetto.findViewById(R.id.icona); //definisco oggetti
            img.setImageResource(nome_corrente.getIDicona());

            TextView Nome = (TextView)oggetto.findViewById(R.id.txtNome);
            Nome.setText(miaLista.get(position).getNome()); // esempio

            TextView Link = (TextView)oggetto.findViewById(R.id.txtLink);
            Link.setText(nome_corrente.getLink());

            return oggetto;

        }


    }

}