package com.example.matteo.verificavuhong;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;

/**
 * Created by Matteo on 06/03/2016.
 */
public class OpenWeb_class extends Activity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webopen);

        Intent intent = getIntent(); // prendo i parametri passati prima
        String Link = intent.getStringExtra("Il nostro valore");

        WebView myWebV = (WebView)findViewById(R.id.WebView);
        WebSettings WebS = myWebV.getSettings();
        WebS.setJavaScriptEnabled(true); // attivo il javascript
        myWebV.loadUrl(Link);

        ImageView Back = (ImageView)findViewById(R.id.imgBack);

       Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
