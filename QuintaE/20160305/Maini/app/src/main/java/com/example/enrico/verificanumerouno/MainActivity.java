package com.example.enrico.verificanumerouno;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    ListView lv;
    Context context;
    ListData[] ObjectItemData = new ListData[3];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int i = 0;

        ObjectItemData[i++] = new ListData("W3C", "http://www.w3schools.com/", R.drawable.android_browser_256);
        ObjectItemData[i++] = new ListData("Google", "https://www.google.it/", R.drawable.android_bluetooth_256);
        ObjectItemData[i++] = new ListData("Meteo", "http://openweathermap.org/", R.drawable.openweathermap_orange_website);

        context = this;
        CustomAdapter adapter = new CustomAdapter(context, R.layout.program_list, ObjectItemData);
        lv = (ListView)findViewById(R.id.ListView1);
        lv.setAdapter(adapter);

    }
}
