package com.example.enrico.verificanumerouno;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

@SuppressWarnings("deprecation")
public class EditActivity extends ActionBarActivity {

	private String name;
	private String desc;
	private String icon;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_list);

		Bundle extras = getIntent().getExtras();

		if (extras != null) {
			desc = extras.getString("Desc");

			WebView myWebView = (WebView) findViewById(R.id.webView);
			WebSettings webs = myWebView.getSettings();
			webs.setJavaScriptEnabled(true);
			myWebView.loadUrl(desc); //Uso l'URL contenuto nella descrizione dell'elemento della lista come parametro da assegnare al caricamento dell'URL da visualizzare nella WebView
			ImageButton ImgButton = (ImageButton)findViewById(R.id.imageButton);
			ImgButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View arg0) {

					finish();

//
				}
			});

		}
	}
	


}
