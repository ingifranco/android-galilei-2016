package com.example.cabrini.verificacabrini;

/**
 * Created by Cabrini on 05/03/2016.
 */
public class collegamento {
    int id_immagine;
    String nome, link;

    public collegamento(int id_immagine, String nome, String link) {
        this.id_immagine = id_immagine;
        this.nome = nome;
        this.link = link;
    }

    public int getId_immagine() {
        return id_immagine;
    }

    public void setId_immagine(int id_immagine) {
        this.id_immagine = id_immagine;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
