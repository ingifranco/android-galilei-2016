package com.example.cabrini.verificacabrini;

import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<collegamento> list = new ArrayList<collegamento>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Riempi_Lista();
        Riempi_ListView();
        GestioneClick();
    }

    private void GestioneClick() {
        ListView lista = (ListView) findViewById(R.id.lista);
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                collegamento oggetto_cliccato = list.get(position);
            }
        });
    }

    private void Riempi_ListView() {
        ArrayAdapter<collegamento>adapter = new Mioadapter();
        ListView lista1 = (ListView) findViewById(R.id.lista);
        lista1.setAdapter(adapter);

    }

    private void Riempi_Lista() {
        list.add(new collegamento(R.drawable.android_browser_256,"w3c","http://www.w3schols.com/"));
        list.add(new collegamento(R.drawable.android_bluetooth_256,"google","http://www.google.it/"));
        list.add(new collegamento(R.drawable.openweathermap_orange_website,"meteo","http://www.openwethermap/"));
    }
    private class Mioadapter extends ArrayAdapter<collegamento> {
        public Mioadapter (){
            super(MainActivity.this,R.layout.estrai_lista,list);
        }
        public View getView (int position, View convertView, ViewGroup parent){
            View oggetto = convertView;
            if (oggetto == null) {
                oggetto = getLayoutInflater().inflate(R.layout.estrai_lista, parent ,false);
            }
            collegamento nome_attuale = list.get(position);

            ImageView img = (ImageView)oggetto.findViewById(R.id.immagine);
            img.setImageResource(nome_attuale.getId_immagine());

            TextView nome = (TextView)oggetto.findViewById(R.id.text_nome);
            nome.setText(list.get(position).getNome());

            TextView link = (TextView)oggetto.findViewById(R.id.txt_link);
            link.setText(nome_attuale.getLink());

            return oggetto;
        }


    }
}
