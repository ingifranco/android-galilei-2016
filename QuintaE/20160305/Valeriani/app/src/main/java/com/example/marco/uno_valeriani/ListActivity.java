package com.example.marco.uno_valeriani;

public class ListActivity {
    // dati membro
    private String Id;
    private String Link;
    private int Icon;

    // contruttore per poter assegnare i valori
    public ListActivity(String Id, String Link, int Icon) {
        this.setId(Id);
        this.setLink(Link);
        this.setIcon(Icon);
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        this.Id = id;
    }

    public String getLink() {
        return Link;
    }

    public void setLink(String link) {
        this.Link = link;
    }

    public int getIcon() {
        return Icon;
    }

    public void setIcon(int icon) {
        this.Icon = icon;
    }

}
