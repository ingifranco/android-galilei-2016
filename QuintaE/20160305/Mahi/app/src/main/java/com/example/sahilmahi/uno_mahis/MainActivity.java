package com.example.sahilmahi.uno_mahis;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    ListView lv; // Dichiaro variabile listview
    Context context;
    ListActivity[] ObjectItemData = new ListActivity[3]; // collezione di oggetti

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int i = 0;
        // passo alla classe l'ID , il Link, e l'Icona
        ObjectItemData[i++] = new ListActivity("W3C", "http://www.w3schools.com/", R.drawable.android_browser_256);
        ObjectItemData[i++] = new ListActivity("Google", "http://www.google.it/", R.drawable.android_bluetooth_256);
        ObjectItemData[i++] = new ListActivity("Meteo", "http://www.openweathermap.org/", R.drawable.openweathermap_orange_website);

        context = this;
        AdapterActivity adapter = new AdapterActivity(context, R.layout.elemento_lista, ObjectItemData);
        lv = (ListView) findViewById(R.id.listView);
        lv.setAdapter(adapter);
    }

}