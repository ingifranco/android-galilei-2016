package com.example.sahilmahi.uno_mahis;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.Toast;

public class WebActivity extends ActionBarActivity {

    // dichiaro le variabili
    WebView myWebView;
    ImageView myImage;
    private String id;
    private String link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        // casting
        myWebView= (WebView) findViewById(R.id.mia_web_view);
        myImage = (ImageView) findViewById(R.id.mio_logo);

        myImage.setImageResource(R.drawable.android_angry_birds_128);

        addImageViewClickListener();
        Bundle extras = getIntent().getExtras();


        if (extras != null) { // se extras è diverso dal valore di NULL
            id = extras.getString("Id"); // recupero l'ID
            link = extras.getString("Link"); // recupero il link http

            // Abilitiamo JavaScript
            WebSettings webs = myWebView.getSettings(); // recupero puntatore
            webs.setJavaScriptEnabled(true); // abilito l'utilizzo

            // L'operatore == non funzione sempre, uso quindi equals().
            if(id.equals("W3C")) {
                // Visualizziamo la pagina Web
                myWebView.loadUrl(link); // Utilizzo in link recuperato dalla classe
            }
            if(id.equals("Google")) {
                // Visualizziamo la pagina Web
                myWebView.loadUrl(link); // Utilizzo in link recuperato dalla classe
            }
            if(id.equals("Meteo")) {
                // Visualizziamo la pagina Web
                myWebView.loadUrl(link); // Utilizzo in link recuperato dalla classe
            }
        }
    }

    public void addImageViewClickListener() {
        myImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
