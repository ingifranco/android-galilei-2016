package com.example.sahilmahi.uno_mahis;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterActivity extends ArrayAdapter<ListActivity> { // listdata non è una classe specifica ma chiunque

    Context mContext; // contesto
    ListActivity[] data; // dati da visualizzare
    int layoutResourceId;

    // costruttore
    public AdapterActivity(Context mContext, int layoutResourceId, ListActivity[] data) {
        // siccome sono una claase derivata..
        super(mContext, layoutResourceId, data);

        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    /// metodo di tipo view che mi passa dei valori
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            // inflate the layout

            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);
        }
        /// va a prendere i dati contenuti nell'array list
        final ListActivity objectItem = data[position];

        /// Va a visualizzare l'icona della lista
        ImageView image = (ImageView) convertView.findViewById(R.id.imageView1);
        image.setImageResource(objectItem.getIcon());

        /// Va a visualizzare l'icona della lista
        TextView textViewItem1 = (TextView) convertView.findViewById(R.id.textView1);
        textViewItem1.setText(objectItem.getId());

        /// Va a visualizzare l'icona della lista
        TextView textViewItem2 = (TextView) convertView.findViewById(R.id.textView2);
        textViewItem2.setText(objectItem.getLink());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Intent intent = new Intent(mContext, WebActivity.class);
                intent.putExtra("Id", objectItem.getId());
                intent.putExtra("Link", objectItem.getLink());

                mContext.startActivity(intent);
            }
        });

        return convertView;

    }
}