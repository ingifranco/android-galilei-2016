package com.example.mirko.verifica_testa;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

@SuppressWarnings("deprecation")
public class EditActivity extends Activity {

	private String name;
	private String desc;
	private String icon;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_list);

		Bundle extras = getIntent().getExtras();

		if (extras != null) {
			icon = extras.getString("Icon");
			name = extras.getString("Name");
			desc = extras.getString("Desc");

			ImageView image = (ImageView) findViewById(R.id.imageView1);
			image.setImageResource(Integer.parseInt(icon));

			TextView textViewItem1 = (TextView) findViewById(R.id.textView1);
			textViewItem1.setText(name);

			TextView textViewItem2 = (TextView) findViewById(R.id.textView2);
			textViewItem2.setText(desc);

		}
	}
	
	public void onClick(View v) {

		final int id = v.getId();

		switch (id) {
		case R.id.imageView1:
			Toast.makeText(getApplicationContext(), "Bye!!!!", Toast.LENGTH_LONG).show();
			break;
		default:
			// your code for button2 here
			break;
		// even more buttons here
		}

		
		finish();
	}

}
