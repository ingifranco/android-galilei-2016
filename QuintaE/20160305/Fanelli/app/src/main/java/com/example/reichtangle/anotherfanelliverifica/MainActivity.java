package com.example.reichtangle.anotherfanelliverifica;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    ListView list;

    String[] web = {"w3c", "google", "meteo"};
    Integer[] imageID = {R.drawable.android_browser_256,
                         R.drawable.android_bluetooth_256,
                         R.drawable.android_newsweather_256};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CustomList adapter = new CustomList(MainActivity.this, web, imageID);
        list = (ListView) findViewById(R.id.list);
        list.setAdapter(adapter);
    }
}
