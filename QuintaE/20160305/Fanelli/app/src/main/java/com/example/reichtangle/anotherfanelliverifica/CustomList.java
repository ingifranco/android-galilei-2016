package com.example.reichtangle.anotherfanelliverifica;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.reichtangle.anotherfanelliverifica.R;

public class CustomList extends ArrayAdapter<String> {
    private final Activity context;
    private final String[] web;
    private final Integer[] imageID;

    public CustomList(Activity context, String[] web, Integer[] imageID) {
        super(context, R.layout.row, web);
        this.context = context;
        this.web = web;
        this.imageID = imageID;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent)
    {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.row, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);
        ImageView imgView = (ImageView) rowView.findViewById(R.id.img);
        txtTitle.setText(this.web[position]);
        imgView.setImageResource(this.imageID[position]);

        return rowView;
    }
}