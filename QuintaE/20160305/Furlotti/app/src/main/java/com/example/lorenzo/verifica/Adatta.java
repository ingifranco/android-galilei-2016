package com.example.lorenzo.verifica;

import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Lorenzo on 05/03/2016.
 */
public class Adatta extends ArrayAdapter<String> {

    private final AppCompatActivity context;
    private final String[] web;
    private final Integer[] image;
    private final String[] URLConn;

    public Adatta(AppCompatActivity context,
                  String[] web, Integer[] image,String[] URLConn) {
        super(context, R.layout.elemento, web);

        this.context = context;
        this.web = web;
        this.image = image;
        this.URLConn = URLConn;

    }
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.elemento, null, true);

        TextView txt = (TextView) rowView.findViewById(R.id.testo);
        TextView txturl = (TextView) rowView.findViewById(R.id.textView);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.img);

        txt.setText(web[position]);
        txturl.setText(URLConn[position]);
        imageView.setImageResource(image[position]);

        return rowView;
    }
}
