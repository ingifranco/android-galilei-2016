package com.example.lorenzo.verifica;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static int itemPosition;
    //URL
    public static String[] URLConn= new String[]{"http://www.w3schools.com/","http://www.google.it/", "http://openweathermap.org/"};
    //Elementi Lista
    String[] web = new String[]{"W3C","Google", "Meteo"};
    //Immagini
    Integer[] image = {
                       R.drawable.android_browser_256,
                       R.drawable.android_bluetooth_256,
                       R.drawable.openweathermap_orange_website};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Dichiarazione lista
        final ListView lista= (ListView)findViewById(R.id.list);
        //Adattamento alla lista
        Adatta adapter = new Adatta(MainActivity.this, web, image, URLConn);
        lista.setAdapter(adapter);

        //Gestione del tocco su un elemento
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // ListView Clicked item index
                itemPosition = position;

                // ListView Clicked item value
                String itemValue = (String) lista.getItemAtPosition(position);

                //Show Alert
                Toast.makeText(getApplicationContext(),
                        "Hai cliccato l'elemento :" + itemPosition + "  ListItem : " + itemValue,
                        Toast.LENGTH_LONG
                ).show();
                // Launching create new product activity
                Intent i = new Intent(getApplicationContext(), WebViewActivity.class);
                startActivity(i);
            }
        });
    }
}
