package com.example.lorenzo.verifica;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * Created by Lorenzo on 05/03/2016.
 */
public class WebViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web);
        //Creo webview
        WebView miosito= (WebView)findViewById(R.id.webView);
        //creo l'image
        ImageView btn = (ImageView) findViewById(R.id.imageView);
        //Metto l'immagine
        btn.setImageResource(R.drawable.android_angry_birds_128);

        //Abilitiamo Javascript
        WebSettings Webs= miosito.getSettings();
        Webs.setJavaScriptEnabled(true);

        miosito. setWebViewClient (new WebViewClient());
        miosito.loadUrl(MainActivity.URLConn[MainActivity.itemPosition]);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Toast.makeText(getApplicationContext(), "Torna al main",
                        Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }
}
