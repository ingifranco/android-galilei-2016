package com.example.donofrios.verifica_donofrio;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    TextView RisX1, RisX2;
    EditText a,b,c;
    Button Cancella,Calcola;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RisX1 = (TextView) findViewById(R.id.RisultatoX1);
        RisX2 = (TextView) findViewById(R.id.RisultatoX2);
        a = (EditText) findViewById(R.id.editTextA);
        b = (EditText) findViewById(R.id.editTextB);
        c = (EditText) findViewById(R.id.editTextC);
        Cancella = (Button) findViewById(R.id.Cancella);
        Calcola = (Button) findViewById(R.id.Calcola);

        Calcola.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strOpA = a.getEditableText().toString();
                String strOpB = b.getEditableText().toString();
                String strOpC = c.getEditableText().toString();
                String strRisX1 = RisX1.getEditableText().toString();
                String strRisX2 = RisX2.getEditableText().toString();

                double dOpA = Double.parseDouble(strOpA);
                double dOpB = Double.parseDouble(strOpB);
                double dOpC = Double.parseDouble(strOpC);
                double dX1  = Double.parseDouble(strRisX1);
                double dX2  = Double.parseDouble(strRisX2);
                double delta;
                if ((strOpA.isEmpty()) || (strOpB.isEmpty()) || (strOpC.isEmpty()))
                {
                    Toast.makeText(getApplication(), "Dati non inseriti" ,Toast.LENGTH_LONG).show();

                }
                else
                {
                    dOpA = Double.parseDouble(strOpA);
                    dOpB = Double.parseDouble(strOpB);
                    dOpC = Double.parseDouble(strOpC);

                    delta = (dOpB*dOpB) - (4*dOpA*dOpC);

                    if(delta < 0){
                        Toast.makeText(getApplicationContext(), "Delta minore di 0" ,Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        dX1 = (-dOpB) + Math.sqrt(delta)/(2*dOpA);
                        RisX1.setText(Double.toString(dX1));
                        dX2 = (-dOpB) + Math.sqrt(delta)/(2*dOpA);
                        RisX2.setText(Double.toString(dX2));

                    }

                }
        }
        });
        Cancella.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                a.setText(" ");
                b.setText(" ");
                c.setText(" ");
                RisX1.setText(" ");
                RisX2.setText(" ");


            }
        });



    }
}
