package com.example.barbieria.verifica_barbieri;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    TextView ris1, ris2;
    EditText test1, test2, test3;
    Button bcalc, bazz;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        test1 = (EditText)findViewById(R.id.editText);
        test2 = (EditText)findViewById(R.id.editText2);
        test3 = (EditText)findViewById(R.id.editText3);
        ris1  = (TextView)findViewById(R.id.textViewris1);
        ris2  = (TextView)findViewById(R.id.textViewris2);
        bcalc = (Button)findViewById(R.id.buttoncalc);
        bazz = (Button)findViewById(R.id.buttonazze);

        bcalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strtest1 = test1.getEditableText().toString();
                String strtest2 = test2.getEditableText().toString();
                String strtest3 = test3.getEditableText().toString();
                if(strtest1.isEmpty() || strtest2.isEmpty() || strtest3.isEmpty())
                {
                    Toast.makeText(getApplicationContext(),
                            "inserire i parametri",
                            Toast.LENGTH_LONG).show();
                }
                else {
                    double dtest1 = Double.parseDouble(strtest1);
                    double dtest2 = Double.parseDouble(strtest2);
                    double dtest3 = Double.parseDouble(strtest3);
                    double delta = dtest2 * dtest2 - 4 * dtest1 * dtest3;
                    if (delta < 0) {
                        Toast.makeText(getApplicationContext(),
                                "il delta é negativo",
                                Toast.LENGTH_LONG).show();
                    }
                    else {
                        double dris1 = (-dtest2 + Math.sqrt(delta)) / (2 * dtest1);
                        double dris2 = (-dtest2 - Math.sqrt(delta)) / (2 * dtest1);
                        String strris1 = Double.toString(dris1);
                        String strris2 = Double.toString(dris2);
                        ris1.setText(strris1);
                        ris2.setText(strris2);
                    }
                }
            }
        });
        bazz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                test1.setText("");
                test2.setText("");
                test3.setText("");
                ris1.setText("");
                ris2.setText("");
            }
        });

    }
}


