package com.example.senti.sentito;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Sentito extends AppCompatActivity {

    EditText A, B, C;
    Button btn1, btn2;
    TextView x1, x2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sentito);


        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        A    = (EditText) findViewById(R.id.a);
        B    = (EditText) findViewById(R.id.b);
        C    = (EditText) findViewById(R.id.c);
        x1   = (TextView) findViewById(R.id.x1);
        x2   = (TextView) findViewById(R.id.x2);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Double v_a = Double.parseDouble(A.getEditableText().toString());
                Double v_b = Double.parseDouble(B.getEditableText().toString());
                Double v_c = Double.parseDouble(C.getEditableText().toString());
                Double delta = (v_b*v_b) - (4*v_a*v_c);

                if (delta < 0 )
                {
                    Toast.makeText(getApplicationContext(),"ATTENZIONE: Determinante negativo",Toast.LENGTH_LONG).show();
                }
                if (delta > 0)
                {
                    double v_x1 = ( -v_b + Math.sqrt(delta)) / (2 * v_a);
                    double v_x2 = ( -v_b - Math.sqrt(delta)) / (2 * v_a);
                    x1.setText(Double.toString(v_x1));
                    x2.setText(Double.toString(v_x2));
                }
            }});

            btn2.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    A.setText("");
                    B.setText("");
                    C.setText("");
                    x1.setText("");
                    x2.setText("");
                }

            });


    }
}

