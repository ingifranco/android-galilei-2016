package com.example.singhr2.singh_verifica;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText edOp_a, edOp_b, edOp_c;
    TextView tvRis_x1,tvRis_x2;
    Button btAzzera, btCalcola;
    double formula;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edOp_a = (EditText)findViewById(R.id.editText);
        edOp_b = (EditText)findViewById(R.id.editText2);
        edOp_c = (EditText)findViewById(R.id.editText3);
        tvRis_x1 = (TextView) findViewById(R.id.textView6);
        tvRis_x2 = (TextView) findViewById(R.id.textView7);
        btAzzera = (Button) findViewById(R.id.button);
        btCalcola = (Button) findViewById(R.id.button2);

        btAzzera.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                edOp_a.setText("");
                edOp_b.setText("");
                edOp_c.setText("");
                tvRis_x1.setText("");
                tvRis_x2.setText("");
            }
        });

        btCalcola.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                String strOp_a = edOp_a.getEditableText().toString();
                String strOp_b = edOp_b.getEditableText().toString();
                String strOp_c = edOp_c.getEditableText().toString();
                double a, b, c;
                double x1, x2;

                if((strOp_a.isEmpty()) || (strOp_b.isEmpty()) || (strOp_c.isEmpty()))
                {
                    Toast.makeText(getApplicationContext(),"Errore!!! I valori inseriti non sono validi", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    a = Double.parseDouble(strOp_a);
                    b = Double.parseDouble(strOp_a);
                    c = Double.parseDouble(strOp_a);

                    double formula = (b * b) - (4 * a * c);

                    if(formula < 0)
                    {
                        Toast.makeText(getApplicationContext(), "Errore!!! Risulato negativo", Toast.LENGTH_SHORT).show();
                        edOp_a.setText("");
                        edOp_b.setText("");
                        edOp_c.setText("");
                        tvRis_x1.setText("");
                        tvRis_x2.setText("");
                    }
                    else
                    {
                        x1 = (-b + Math.sqrt(formula))/(2 * a);
                        x2 = (-b - Math.sqrt(formula))/(2 * a);
                        tvRis_x1.setText(Double.toString(x1));
                        tvRis_x2.setText(Double.toString(x2));
                    }

                }
            }
        });
    }
}
