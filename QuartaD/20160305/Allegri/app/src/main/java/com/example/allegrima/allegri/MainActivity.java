package com.example.allegrima.allegri;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{
    EditText t_a=(EditText)findViewById(R.id.A);
    EditText t_b=(EditText)findViewById(R.id.B);
    EditText t_c=(EditText)findViewById(R.id.C);
    TextView t_x1=(TextView)findViewById(R.id.x1);
    TextView t_x2=(TextView)findViewById(R.id.x2);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
// Funzione inguardabile
    private void Azzera() /**************************/
    {
        t_a.setText("");
        t_b.setText("");
        t_c.setText("");
        t_x1.setText("");
        t_x2.setText("");
    }

    private void Calcola()
    {
        double a,b,c;
        double delta;
        double x1,x2;

        a = Double.parseDouble(t_a.toString());
        b = Double.parseDouble(t_b.toString());
        c = Double.parseDouble(t_c.toString());

        delta=(b*b)-4*a*c;
        if(delta<0)
        {
            Toast.makeText(getApplicationContext(),"ERRORE: determinante negativo",Toast.LENGTH_SHORT).show();
        }
        else
        {
            if((t_a.length()==0)||(t_b.length()==0)||(t_c.length()==0))
            {
                Toast.makeText(getApplicationContext(),"invalid double",Toast.LENGTH_SHORT).show();
            }
            else
            {
                x1=((-b)+Math.sqrt(delta))/(2*a);
                x2=((-b)-Math.sqrt(delta))/(2*a);

                String s_x1=Double.toString(x1);
                String s_x2=Double.toString(x2);
                t_x1.setText(s_x1);
                t_x2.setText(s_x2);
            }
        }
    }
}
