package com.example.vassallog.verifica_05032016;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Verifica extends AppCompatActivity {

    Button calcola;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verifica);

        calcola = (Button)findViewById(R.id.calcola);
        cancella = (Button)findViewById(R.id.clcola);

        calcola.SetOnClickListener(new view.ConClickListener)
        {
            double a,b,c,x1,x2;
            double determinante;

            @override
            public void ONClick(view v)
            {
                string testo_a = a.GetEditableText().ToString()
                string testo_b = b.GetEditableText().ToString();
                string testo_c = c.GetEditableText().ToString();

                if((testo_a.IsEmpty()) ||(testo_b.IsEmpty()) ||(testo_c.IsEmpty()))
                {
                    Toast.MakeText(GetApplicationContext()."valori non validi",Toast.Length_short).show();
                }
                else
                {
                    double valore_a = double.ParseDouble(testo_a);
                    double valore_b = double.ParseDouble(testo_b);
                    double valore_c = double.ParseDouble(testo_c);

                    double det =(valore_b*valore_b)-(4*valore_a*valore_c);

                    if(det<0)
                    {
                     Toast.Maketext(GetApplicationContext(),"determinante negativo",Toast.Length_short).show();
                        a.SetText("");
                        b.SetText("");
                        c.SetText("");
                        x1.SetText("");
                        x2.SetText("");
                    }
                    else
                    {
                        double valore_x1 = ( - valore_b + math.sqrt(determinante))/(2*valore_a)
                        double valore_x2 = ( - valore_b - math.sqrt(determinante))/(2*valore_b)

                        x1.SetText(double.ToString(vvalore_x1));
                        x2.SetText(double.ToString(vvalore_x2));
                    }
                }
            }
        };

        cancella.SetOnClickListener(new view.OnClickListener())
        {
            @override
                    public void OnClicki(view v)
            {
                a.SetText("");
                b.SetText("");
                c.SetText("");
                x1.SetText("");
                x2.SetText("");
            }
        }
    }


}
