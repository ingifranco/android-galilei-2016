package com.example.cavalcas.verifica;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Verifica4D extends AppCompatActivity {
   EditText a;
   EditText b;
   TextView titolo;
   EditText c;
   TextView numero1;
   TextView numero2;
   TextView numero3;
   TextView TextRis;
   TextView TextRis2;
   TextView Ris1;
   TextView Ris2;
   Button buttonCanc;
   Button buttonCalc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verifica4_d);


        a= (EditText)findViewById(R.id.editnum1);
        b = (EditText)findViewById(R.id.editnum2);
        c = (EditText)findViewById(R.id.editnum3);
        buttonCalc = (Button) findViewById(R.id.buttonCalc);
        buttonCanc = (Button) findViewById(R.id.buttonCanc);
        titolo = (TextView) findViewById(R.id.titolo);
        numero1 = (TextView)findViewById(R.id.numero1);
        numero2 = (TextView)findViewById(R.id.numero2);
        numero3 = (TextView)findViewById(R.id.numero3);
        TextRis = (TextView)findViewById(R.id.TextRis);
        TextRis2 = (TextView)findViewById(R.id.TextRis2);
        Ris1 = (TextView)findViewById(R.id.Ris1);
        Ris2 = (TextView)findViewById(R.id.Ris2);

        buttonCanc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                a.setText("");
                b.setText("");
                c.setText("");
                Ris1.setText("");
                Ris2.setText("");
            }
        });
        buttonCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               String stra = a.getEditableText().toString();
               String strb = b.getEditableText().toString();
               String strc = c.getEditableText().toString();
                double a, b, c;
                double Ris1, Ris2;

                if (stra.isEmpty()) || (strb.isEmpty()) || (strc.isEmpty())
                {
                    Toast.makeText(getApplicationContext()," Error!",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    a = Double.parseDouble(stra);
                    b = Double.parseDouble(strb);
                    c = Double.parseDouble(strc);

                    double equazione = (b*b) - (4 * a *c );

                    if(equazione < 0)
                    {
                        Toast.makeText(getApplicationContext(),"Error 2! ",Toast.LENGTH_SHORT ).show();





                    }
                    else
                    {
                        Ris1 = (-b + Math.sqrt(equazione))/(2 * a);
                        Ris2 = (-b + Math.sqrt(equazione))/(2 * a);


                    }
                }
            }
        });


    }




}
