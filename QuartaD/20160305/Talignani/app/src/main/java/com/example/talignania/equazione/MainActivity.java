package com.example.talignania.equazione;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btCalc, btCanc;
        TextView x1, x2;
        EditText a, b, c;

        btCalc = (Button)findViewById(R.id.button2);
        btCanc = (Button)findViewById(R.id.button);
        x1 = (TextView)findViewById(R.id.textView8);
        x2 = (TextView)findViewById(R.id.textView7);
        a = (EditText)findViewById(R.id.editText);
        b = (EditText)findViewById(R.id.editText2);
        c = (EditText)findViewById(R.id.editText3);

        btCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String str_a = a.getEditableText().toString();
                String str_b = b.getEditableText().toString();
                String str_c = c.getEditableText().toString();

                if((str_a.isEmpty())||(str_b.isEmpty())||(str_c.isEmpty()))
                {
                   Toast.makeText(getApplicationContext(),"ERRORE:Valori inseriti non validi",Toast.LENGTH_SHORT).show();
                }
                else
                {

                    double val_a = Double.parseDouble(str_a);
                    double val_b = Double.parseDouble(str_b);
                    double val_c = Double.parseDouble(str_c);

                    double determinante = (val_b * val_b) - (4 * val_a * val_c);

                    if (determinante < 0) {
                        Toast.makeText(getApplicationContext(), "Attenzione:Determinante Negativo", Toast.LENGTH_SHORT).show();
                        a.setText("");
                        b.setText("");
                        c.setText("");
                        x1.setText("");
                        x2.setText("");
                    } else {
                        double val_x1 = (-val_b + Math.sqrt(determinante))/(2 * val_a);
                        double val_x2 = (-val_b - Math.sqrt(determinante))/(2 * val_a);
                        x1.setText(Double.toString(val_x1));
                        x2.setText(Double.toString(val_x2));
                    }
                }
            }
        });

        btCanc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                a.setText("");
                b.setText("");
                c.setText("");
                x1.setText("");
                x2.setText("");
            }
        });
    }

    
}
