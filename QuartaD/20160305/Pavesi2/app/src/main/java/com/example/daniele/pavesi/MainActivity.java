package com.example.daniele.pavesi;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btcalcola, btazzera;
        TextView x1 , x2;
        EditText a,b,c;

        btcalcola = (Button)findViewById(R.id.calcola);
        btazzera = (Button)findViewById(R.id.azzera);
        x1 = (TextView)findViewById(R.id.Ris x1);
        x2 = (TextView)findViewById(R.id.Ris x2);
        a = (EditText)findViewById(R.id.a);
        b = (EditText)findViewById(R.id.b);
        c = (EditText)findViewById(R.id.c);

        btcalcola.setOnClickListener (new View.OnClickListener(){
            @Override
            public void OnClick(View v){
                String s_a = a.getEditableText().toString();
                String s_b = b.getEditableText().toString();
                String s_c = c.getEditableText().toString();


                if((s_a.isEmpty())||(s_b.isEmpty())||(s_c.isEmpty()))
                {
                    Toast.makeText(getApplicationContext(),"Valori inseriti errati!", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    double v_a =  Double.ParseDouble(s_a);
                    double v_b =  Double.ParseDouble(s_b);
                    double v_c =  Double.ParseDouble(s_c);
                    double det = (v_b * v_b) - (4 * v_a * v_c);

                    if(det < 0) {
                        Toast.makeText(getApplicationContext(),"Determinante negativo!", Toast.LENGTH_SHORT).show();
                        a.setText("");
                        b.setText("");
                        c.setText("");
                        x1.setText("");
                        x2.setText("");
                    }
                    else
                    {
                        double v_x1 = (-v_b + Math.sqrt(det))/(2 * v_a);
                        double v_x2 = (-v_b - Math.sqrt(det))/(2 * v_a);
                        x1.SetText(Double.toString(v_x1));
                        x2.SetText(Double.toString(v_x2));
                    }
                }
            }
        });

        btazzera.setOnClickListener(new View.OnClickListener()) {
            @Override
                    public void OnClick(View v) {

                    a.setText("");
                    b.setText("");
                    c.setText("");
                    x1.setText("");
                    x2.setText("");
            }
        });
    }
}
