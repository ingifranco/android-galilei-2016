package com.example.sabertooth.cattani;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText eda, edb, edc;
    TextView lx1, lx2;
    Button calcola, btazzera;
    double ris;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        eda = (EditText) findViewById(R.id.a);
        edb = (EditText) findViewById(R.id.b);
        edc = (EditText) findViewById(R.id.c);
        lx1 = (TextView) findViewById(R.id.x1);
        lx2 = (TextView) findViewById(R.id.x2);
        calcola = (Button) findViewById(R.id.calcola);
        btazzera = (Button) findViewById(R.id.azzera);

        btazzera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eda.setText("");
                edb.setText("");
                edc.setText("");
                lx1.setText("");
                lx2.setText("");

            }
        });

        calcola.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str_a = eda.getEditableText().toString();
                String str_b = edb.getEditableText().toString();
                String str_c = edc.getEditableText().toString();
                double a=0, b, c, x1, x2;
                if ((str_a.isEmpty()) || (str_b.isEmpty()) || (str_c.isEmpty()))
                    Toast.makeText(getApplicationContext(), "Attenzione nonhaiinserito niente", Toast.LENGTH_LONG).show();
                else

                a = Double.parseDouble(str_a);
                b = Double.parseDouble(str_b);
                c = Double.parseDouble(str_c);
                ris = (b * b) - (4 * a * c);
                if (ris < 0) {
                    Toast.makeText(getApplicationContext(), "Il delta è negativo", Toast.LENGTH_LONG).show();
                    eda.setText("");
                    edb.setText("");
                    edc.setText("");
                    lx1.setText("");
                    lx2.setText("");
                } else {
                    x1 = (-b+Math.sqrt(ris)/(2*a));
                    x2 = (-b-Math.sqrt(ris)/(2*a));
                    lx1.setText(Double.toString(x1));
                    lx2.setText(Double.toString(x2));



                }

            }
        });


    }
}


