package com.example.zorilai.verifica_zorila;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText edA, edB, edC;
    TextView tvX1, tvX2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edA = (EditText) findViewById(R.id.editText);
        edB = (EditText) findViewById(R.id.editText2);
        edC = (EditText) findViewById(R.id.editText3);
        tvX1 = (TextView) findViewById(R.id.textView6);
        tvX2 = (TextView) findViewById(R.id.textView7);
    }

    public void Azzera (View v)
    {
        edA.setText("");
        edB.setText("");
        edC.setText("");
        tvX1.setText("");
        tvX1.setText("");
    }

    public void Calcola (View v)
    {
        double dedA,dedB,dedC,delta;
        double dtvX1,dtvX2;

        dedA = Double.parseDouble(edA.getText().toString());
        dedB = Double.parseDouble(edB.getText().toString());
        dedC = Double.parseDouble(edC.getText().toString());

        if((edA.length() != 0)||(edB.length() != 0)||(edC.length() != 0))
        {
            delta = Math.sqrt((dedB*dedB)-(4*(dedA*dedC)));
            if(delta > 0)
            {
                dtvX1 = (-dedB*(delta))/(2*dedA);
                tvX1.setText(""+dtvX1);
                dtvX2 = (-dedB*(-delta))/(2*dedA);
                tvX2.setText(""+dtvX2);
            }
            else
                Toast.makeText(this,"Delta minore di zero",Toast.LENGTH_LONG).show();
        }
        else
            Toast.makeText(this,"Non sono stati inseriti tutti i termini",Toast.LENGTH_LONG).show();
    }
}
