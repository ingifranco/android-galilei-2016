package com.example.deluciag.verifica_delucia;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button bt_azzera, bt_calcola;
    EditText editText_a, editText_b, editText_c;
    TextView ris1,ris2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // manca la findViewById .....
        ris1.setText("");
        ris2.setText("");

        bt_azzera = (Button) findViewById(R.id.bt_azzera);
        bt_calcola= (Button) findViewById(R.id.bt_calcola);

        editText_a = (EditText)findViewById(R.id.editText_a);
        editText_b = (EditText)findViewById(R.id.editText_b);
        editText_c = (EditText)findViewById(R.id.editText_c);

        bt_azzera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText_a.setText("");
                editText_b.setText("");
                editText_c.setText("");
            }
        });

        bt_calcola.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int l_a = editText_a.length();
                int l_b = editText_b.length();
                int l_c = editText_c.length();

                String str_a = editText_a.getText().toString();
                String str_b = editText_b.getText().toString();
                String str_c = editText_c.getText().toString();

                double a = Double.parseDouble(str_a);
                double b = Double.parseDouble(str_b);
                double c = Double.parseDouble(str_c);

                double d = Math.sqrt(Math.pow(b, 2) - 4 * a * c);

                if (d<0)
                {
                    Toast.makeText(getApplicationContext(), "ATTENZIONE: determinante negativo!", Toast.LENGTH_LONG).show();
                }
                else {
                    double x1 = (-d - b) / (2 * a);
                    double x2 = (d - b) / (2 * a);

                    String str_ris1 = Double.toString(x1);
                    String str_ris2 = Double.toString(x2);

                    ris1.setText(str_ris1);
                    ris2.setText(str_ris2);
                }
            }
        });
    }
}
