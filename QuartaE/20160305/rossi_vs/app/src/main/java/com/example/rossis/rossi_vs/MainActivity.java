package com.example.rossis.rossi_vs;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText a1,b1,c1,x111,x222;
    Button bt1,bt2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bt1=(Button)findViewById(R.id.btcalcola);
        bt2=(Button)findViewById(R.id.btazzera);
        a1=(EditText)findViewById(R.id.a);
        b1=(EditText)findViewById(R.id.b);
        c1=(EditText)findViewById(R.id.c);
        x111=(EditText)findViewById(R.id.x11);
        x222=(EditText)findViewById(R.id.x22);

        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String a2,b2,c2;
                a2= a1.getText().toString();
                b2= b1.getText().toString();
                c2= c1.getText().toString();
                Double a3=Double.parseDouble(a2);
                Double b3=Double.parseDouble(b2);
                Double c3=Double.parseDouble(c2);

                //
                Double delta=(b3*b3)+4*(a3*c3); // formula sbagliata
                if(delta>=0)
                {
                    Double x1,x2;
                    x1=(-b3+Math.sqrt(delta))/2;
                    x2=(-b3-Math.sqrt(delta))/2;
                    x111.setText(x1.toString());
                    x111.setText(x2.toString()); // destinazione errata
                }
                else
                {
                    x111.setText("IMPOSSIBILE");
                    x222.setText("IMPOSSIBILE");
                }
            }
        });
        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                a1.setText("");
                b1.setText("");
                c1.setText("");
                x111.setText("");
                x222.setText("");
            }
        });
    }
}
