package com.example.ippolitok.verifica;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public class Calcolatrice
    {
        public static void main(String[] args){
            CalcolatriceFinestra calcolatriceFinestra = new CalcolatriceFinestra("Calcolatrice");
            calcolatriceFinestra.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            calcolatriceFinestra.setSize(300,300);
            calcolatriceFinestra.setVisible(true);
        }
    }
    public class CalcolatriceFinestra extends JFrame {
        private JTextField display;
        private CalcolatricePannello    bottoniera;

        public CalcolatriceFinestra(String titolo) {
            super(titolo);
            display = new JTextField();
            bottoniera = new CalcolatricePannello(this);

            add(display,BorderLayout.NORTH);
            add(bottoniera,BorderLayout.CENTER);

            display.setEditable(false);
            display.setFont(new Font("SansSerif",Font.ITALIC,22));
            display.setHorizontalAlignment(JTextField.RIGHT);
            display.setBackground(Color.LIGHT_GRAY);

        }

}
