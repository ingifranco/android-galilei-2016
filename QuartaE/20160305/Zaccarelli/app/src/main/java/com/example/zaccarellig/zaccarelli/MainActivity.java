package com.example.zaccarellig.zaccarelli;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button   BT_Azzera,BT_Calcola;
    TextView LB_A,LB_B,LB_C,LB_X1,LB_X2;
    EditText ED_A, ED_B,ED_C,ED_X1,ED_X2;
    String   A,B,C;
    Double   D_A,D_B,D_C,D_X1,D_X2,Delta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BT_Azzera = (Button) findViewById(R.id.button);
        BT_Calcola = (Button) findViewById((R.id.button2));
        LB_A = (TextView) findViewById(R.id.textView);
        LB_B = (TextView) findViewById(R.id.textView2);
        LB_C = (TextView) findViewById(R.id.textView3);
        LB_X1 = (TextView) findViewById(R.id.textView4);
        LB_X2 = (TextView) findViewById(R.id.textView5);
        ED_A = (EditText) findViewById(R.id.editText);
        ED_B = (EditText) findViewById(R.id.editText2);
        ED_C = (EditText) findViewById(R.id.editText3);
        A = ED_A.getText().toString();
        B = ED_B.getText().toString();
        C = ED_C.getText().toString();
        D_A = Double.parseDouble(A);
        D_B = Double.parseDouble(B);
        D_C = Double.parseDouble(C);

        Delta = (D_B * D_B) + 4 * (D_A * D_A); // formula sbagliata

        if (Delta >= 0) {
            D_X1 = (-D_B + Math.sqrt(Delta)) / 2; // formula sbagliata
            D_X2 = (-D_B - Math.sqrt(Delta)) / 2; // formula sbagliata
            LB_X1.setText(D_X1.toString());
            LB_X2.setText(D_X2.toString());
        } else {
            Toast.makeText(getApplicationContext(),
                    "ATTENZIONE:Determinante Negativo", Toast.LENGTH_LONG).show();
        }
    BT_Azzera.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            LB_A.setText("");
            LB_B.setText("");
            LB_C.setText("");
            LB_X1.setText("X1:");
            LB_X2.setText("X2:");
        }
    });
        BT_Calcola.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ED_A.getText().toString().trim().length() > 0 &&
                        ED_B.getText().toString().trim().length() > 0 &&
                        ED_C.getText().toString().trim().length() > 0) {
                    Delta = (D_B * D_B) + 4 * (D_A * D_A);

                    if (Delta >= 0) {
                        D_X1 = (-D_B + Math.sqrt(Delta)) / 2;
                        D_X2 = (-D_B - Math.sqrt(Delta)) / 2;
                        LB_X1.setText(D_X1.toString());
                        LB_X2.setText(D_X2.toString());
                    } else {
                        Toast.makeText(getApplicationContext(),
                                "ATTENZIONE:Determinante Negativo", Toast.LENGTH_LONG).show();
                    }
                }
                    else {
                    Toast.makeText(getApplicationContext(),
                        "Invalid double", Toast.LENGTH_LONG).show();
                }
            }
        });

    }
}


 