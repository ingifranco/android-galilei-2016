package com.example.kumark.kumarvs;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button button1,button2;
    EditText editop1,editop2,editop3;
    TextView risx1,risx2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editop1 = (EditText)findViewById(R.id.editText1);
        editop2 = (EditText)findViewById(R.id.editText2);
        editop3 = (EditText)findViewById(R.id.editText3);

        risx1 = (TextView)findViewById(R.id.textView6);
        risx2 = (TextView)findViewById(R.id.textView7);

        button1=(Button)findViewById(R.id.bt1);
        button2=(Button)findViewById(R.id.bt2);

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                        if((editop1.getText().toString().trim().length() > 0)&&(editop2.getText().toString().trim().length() > 0)&&(editop3.getText().toString().trim().length() > 0))
                        {
                            String strOp1 = editop1.getText().toString();
                            String strOp2 = editop2.getText().toString();
                            String strOp3 = editop3.getText().toString();

                            Double a = Double.parseDouble(strOp1);
                            Double b = Double.parseDouble(strOp2);
                            Double c = Double.parseDouble(strOp3);
                            Double s = (b*b) -(4*a*c);
                            Double d1 = Math.sqrt(s);

                            if(d1 < 0) // occorre verificare s, non d1
                            {
                                Toast.makeText(getApplicationContext(),"ATTTENZIONE: determinante negativo",Toast.LENGTH_LONG).show();
                                editop1.setText(" ");
                                editop2.setText(" ");
                                editop3.setText(" ");
                            }
                            else
                            {
                                Double r1 = (-b+d1)/(2*a);
                                Double r2 = (-b-d1)/(2*a);
                                risx1.setText(Double.toString(r1));
                                risx2.setText(Double.toString(r2));
                            }

                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(),
                                    "Invalid Double:'' ",
                                    Toast.LENGTH_LONG).show();

                        }
            }
        });

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editop1.setText(" ");
                editop2.setText(" ");
                editop3.setText(" ");
                risx1.setText(" ");
                risx2.setText(" ");
            }
        });

    }
}
