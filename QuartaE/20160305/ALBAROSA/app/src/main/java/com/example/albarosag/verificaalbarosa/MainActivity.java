package com.example.albarosag.verificaalbarosa;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button bottone_calcola,bottone_azzera;
    EditText ED_a,ED_b,ED_c;
    TextView TextView_1,TextView_2,TextView_3,TX_ris_x1,TX_ris_x2,TX_errore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ED_a=(EditText)findViewById(R.id.ED1);
        ED_b=(EditText)findViewById(R.id.ED2);
        ED_c=(EditText)findViewById(R.id.ED3);
        bottone_calcola=(Button)findViewById(R.id.BT_calcola);
        TextView_1=(TextView)findViewById(R.id.TX1);
        TextView_2=(TextView)findViewById(R.id.TX2);
        TextView_3=(TextView)findViewById(R.id.TX3);
        TX_ris_x1=(TextView)findViewById(R.id.TX_x1);
        TX_ris_x2=(TextView)findViewById(R.id.TX_x2);
        TX_errore=(TextView)findViewById(R.id.TX_err);
        bottone_azzera=(Button)findViewById(R.id.BT_azzera);

        bottone_calcola.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String str1=ED_a.getText().toString();
                String str2=ED_b.getText().toString();
                String str3=ED_c.getText().toString();
                double a=Double.parseDouble(str1);
                double b=Double.parseDouble(str2);
                double c=Double.parseDouble(str3);
                double delta=(b*b)-4*a*c;
                if(delta<0)
                {
                    Toast.makeText(getApplicationContext(),
                            "DELTA NEGATIVO:'' ",
                            Toast.LENGTH_LONG).show();
                }
                else {
                    double X1 = (-b + Math.sqrt(delta)) / 2 * a; // formula sbagliata
                    double X2 = (-b - Math.sqrt(delta)) / 2 * a;
                    TX_ris_x1.setText(Double.toString(X1));
                    TX_ris_x2.setText(Double.toString(X2));
                }

            }
        });

        bottone_azzera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ED_a.setText("");
                ED_b.setText("");
                ED_c.setText("");
            }
        });
    }
}
