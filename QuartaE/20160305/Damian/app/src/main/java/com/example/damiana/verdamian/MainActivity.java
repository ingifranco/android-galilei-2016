package com.example.damiana.verdamian;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView a,b,c,x1,x2,r1,r2;
    EditText t1,t2,t3;
    Button b1,b2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b1=(Button)findViewById(R.id.BT_Azzera);
        b2=(Button)findViewById(R.id.BT_Calcola);

        a=(TextView)findViewById(R.id.TV_a);
        b=(TextView)findViewById(R.id.TV_b);
        c=(TextView)findViewById(R.id.TV_c);
        x1=(TextView)findViewById(R.id.TV_x1);
        x2=(TextView)findViewById(R.id.TV_x2);

        r1=(TextView)findViewById(R.id.TV_r1);
        r2=(TextView)findViewById(R.id.TV_r2);

        t1=(EditText)findViewById(R.id.PT1);
        t2=(EditText)findViewById(R.id.PT2);
        t3=(EditText)findViewById(R.id.PT3);


        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                r1.setText(" ");
                r2.setText(" ");
            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                double a=Double.parseDouble(t1.getText().toString());
                double b=Double.parseDouble(t2.getText().toString());
                double c=Double.parseDouble(t3.getText().toString());
                double ris1=0;
                double ris2=0;
                double delta=0;

                delta = (b*b)-(4*a*c);
                // manca il controllo del determinante (delta)
                ris1= (-(b) + Math.sqrt(delta))/(2*a);
                ris2= (b-Math.sqrt(delta)/(2*a));

                String str1 = Double.toString(ris1);
                String str2 = Double.toString(ris2);


                r1.setText(str1);
                r2.setText(str2);



            }
        });

    }
}
