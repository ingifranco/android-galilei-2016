package com.example.lodir.lodiver;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Double aa,bb,cc,x_uno,x_due;
    Button Calcola,Azzera;
    EditText E_A,E_B,E_C;
    TextView risUno,risDue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Calcola=(Button)findViewById(R.id.BT_Calcola);
        Azzera=(Button)findViewById(R.id.BT_Azzera);
        risUno=(TextView)findViewById(R.id.TV_Ris1);
        risDue=(TextView)findViewById(R.id.TV_Ris2);
        E_A=(EditText)findViewById(R.id.ET_A);
        E_B=(EditText)findViewById(R.id.ET_B);
        E_C=(EditText)findViewById(R.id.ET_C);

    Calcola.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            aa = Double.parseDouble(E_A.getText().toString());
            bb = Double.parseDouble(E_B.getText().toString());
            cc = Double.parseDouble(E_C.getText().toString());

            x_uno=((-bb)+Math.sqrt(bb*bb-4*aa*cc))/2*aa; // formula sbagliata
            x_uno=((-bb)-Math.sqrt(bb*bb-4*aa*cc))/2*aa;
            risUno.setText(x_uno.toString());
            risDue.setText(x_due.toString());
        }
    });

        Azzera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                E_A.setText("");
                E_B.setText("");
                E_C.setText("");
                risDue.setText("");
                risUno.setText("");
            }
        });
    }
}
