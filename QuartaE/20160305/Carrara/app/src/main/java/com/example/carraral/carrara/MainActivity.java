package com.example.carraral.carrara;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText A, B, C;
    TextView X1, X2;
    Button Azzera, Calcola;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        A = (EditText) findViewById(R.id.Num_A);
        B = (EditText) findViewById(R.id.Num_B);
        C = (EditText) findViewById(R.id.Num_C);
        X1 = (TextView) findViewById(R.id.x1);
        X2 = (TextView) findViewById(R.id.x2);
        Azzera = (Button) findViewById(R.id.azzera);
        Calcola = (Button) findViewById(R.id.calcola);


        Calcola.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Risoluzione Equazione", Toast.LENGTH_LONG).show();

                String strA = A.getText().toString();
                String strB = B.getText().toString();
                String strC = C.getText().toString();
                int a = Integer.parseInt(strA);
                int b = Integer.parseInt(strB);
                int c = Integer.parseInt(strC);

                double x_1 = -b + Math.sqrt(b * b - 4 * a * c) / 2*a; // formula sbagliata
                double x_2 = -b - Math.sqrt(b*b - 4*a*c ) / 2*a;

                X1.setText(Double.toString(x_1));
                X2.setText(Double.toString(x_2));

            }
        });

        Azzera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Azzero", Toast.LENGTH_LONG).show();

                A.setText(" ");
                B.setText(" ");
                C.setText(" ");
                X1.setText(" ");
                X2.setText(" ");

            }
        });


    }
}
