package com.example.ingfr.rubens;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

public class MainActivity extends AppCompatActivity {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    EditText editop1, editop2, editop3, ris1, ris2;
    Button bt1;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editop1 = (EditText) findViewById(R.id.editText1);
        editop2 = (EditText) findViewById(R.id.editText2);
        editop3 = (EditText) findViewById(R.id.editText3);
        ris1 = (EditText) findViewById(R.id.editText4);
        ris2 = (EditText) findViewById(R.id.editText5);

        bt1 = (Button) findViewById(R.id.Bt_calcola);

        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str1 = editop1.getText().toString();
                String str2 = editop2.getText().toString();
                String str3 = editop3.getText().toString();

                double a= Double.parseDouble(str1);
                double b = Double.parseDouble(str2);
                double c = Double.parseDouble(str3);

                double delta = (b*b)-4*(a*c);

                double x1= (-b+Math.sqrt(delta))/2;
                double x2= (-b-Math.sqrt(delta))/2;

                ris1.setText(double.class.toString(x1);
                ris2.setText(double.class.toString(x2);
            }

        });

    }


