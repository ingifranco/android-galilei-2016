package com.example.agnellil.agnelli_vs1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {
    Button Button1,Button2;
    EditText editop1,editop2,editop3;
    TextView ris1,ris2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editop1 = (EditText)findViewById(R.id.editText1);
        editop2 = (EditText)findViewById(R.id.editText2);
        editop3 = (EditText)findViewById(R.id.editText3);

        ris1 = (TextView)findViewById(R.id.x1);
        ris2 = (TextView)findViewById(R.id.x2);

        Button1=(Button)findViewById(R.id.bt1);
        Button2=(Button)findViewById(R.id.bt2);

        Button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str1 = editop1.getText().toString();// il copia incolla è "pericoloso"
                String str2 = editop1.getText().toString();
                String str3 = editop1.getText().toString();

                double a = Double.parseDouble(str1);
                double b = Double.parseDouble(str2);
                double c = Double.parseDouble(str3);

                double op = (b*b) - 4*a*c;

                double x1 = (-b+Math.sqrt(op)/2*a); // formula sbagliata
                double x2 = (-b-Math.sqrt(op)/2*a);

                ris1.setText(Double.toString(x1));
                ris2.setText(Double.toString(x2));



            }
        });


    }
}
