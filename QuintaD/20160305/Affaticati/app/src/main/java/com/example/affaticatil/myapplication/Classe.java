package com.example.affaticatil.myapplication;

/**
 * Created by affaticatil on 04/03/2016.
 */
public class Classe {
    String nome;
    String sito;
    int immagine;

    public Classe(String nome, String sito, int immagine){
        this.nome = nome;
        this.sito = sito;
        this.immagine = immagine;
    }

    public String get_nome(){
        return this.nome;
    }

    public String get_sito(){
        return this.sito;
    }

    public int get_immagine(){
        return this.immagine;
    }
}
