package com.example.affaticatil.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.List;


import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private List<Classe> lista = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        riempilista();
        riempiListView();
        gestioneClick();

    }


    public void riempilista() {
        lista.add(new Classe("W3C", "http://www.w3schools.com/", R.drawable.android_browser_256));
        lista.add(new Classe("Google", "http://www.google.it/", R.drawable.android_bluetooth_256));
        lista.add(new Classe("Meteo", "http://www.openweathermap.com/", R.drawable.openweathermap_orange_website));
    }

    private void riempiListView() {
        ArrayAdapter<Classe> adapter = new MioAdapter();
        ListView lista = (ListView)findViewById(R.id.listView);
        lista.setAdapter(adapter);
    }

    private void gestioneClick() {
        final ListView listaw = (ListView)findViewById(R.id.listView);
        listaw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View viewClicked,
                                    int position, long id) {
                Classe OggettoCliccato = lista.get(position);
                Intent i = new Intent(MainActivity.this,Activity2.class);
                i.putExtra("sito",OggettoCliccato.get_sito());
                startActivity(i);
            }
        });

    }

    private class MioAdapter extends ArrayAdapter<Classe>{

        public MioAdapter() {
            super(MainActivity.this, R.layout.da_lista,lista);
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            View oggetto = convertView;
            if (oggetto == null){
                oggetto = getLayoutInflater().inflate(R.layout.da_lista, parent, false);
            }
            Classe sito_corrente = lista.get(position);

            ImageView img = (ImageView)oggetto.findViewById(R.id.imageView);
            img.setImageResource(sito_corrente.immagine);

            TextView sito = (TextView)oggetto.findViewById(R.id.textView);
            sito.setText(sito_corrente.get_sito());

            TextView nome= (TextView)oggetto.findViewById(R.id.textView2);
            nome.setText(sito_corrente.get_nome());

            return oggetto;

        }
}
}
