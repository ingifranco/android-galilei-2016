package com.example.affaticatil.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;


import android.webkit.WebSettings;
import android.webkit.WebView;


public class Activity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity2);

        String sito = getIntent().getStringExtra("sito");
        WebView myweb = (WebView) findViewById(R.id.webView);
        WebSettings webs = myweb.getSettings();
        webs.setJavaScriptEnabled(true);
        myweb.loadUrl(sito);
    }
}