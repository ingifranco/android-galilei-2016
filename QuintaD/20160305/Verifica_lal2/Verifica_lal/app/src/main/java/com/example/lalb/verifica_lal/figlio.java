package com.example.lalb.verifica_lal;

/**
 * Created by lalb on 04/03/2016.
 */
public class figlio {
    String nome1,logo1;
    int icona1;

    public figlio(String nome1, String logo1, int icona1) {
        this.nome1 = nome1;
        this.logo1 = logo1;
        this.icona1 = icona1;
    }

    public String getNome1() {
        return nome1;
    }

    public void setNome1(String nome1) {
        this.nome1 = nome1;
    }

    public String getLogo1() {
        return logo1;
    }

    public void setLogo1(String logo1) {
        this.logo1 = logo1;
    }

    public int getIcona1() {
        return icona1;
    }

    public void setIcona1(int icona1) {
        this.icona1 = icona1;
    }
}

