package com.example.continil.verificacontini;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;


/**
 * Created by Lorenzo on 04/03/2016.
 */
public class Pagina extends AppCompatActivity{
    String val;
    WebView myWebV;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = getIntent();
        String Indirizzo = intent.getStringExtra(val);

        myWebV = (WebView)findViewById(R.id.WV_Sito);

        myWebV = (WebView) myWebV.findViewById(R.id.WV_Sito);
        //Abilito Javascript
        WebSettings webS = myWebV.getSettings();
        webS.setJavaScriptEnabled(true);
        myWebV.loadUrl(Indirizzo);

        ImageButton btn = (ImageButton)findViewById(R.id.BTN_Esci);
        btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent intent = new Intent(Pagina.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    }