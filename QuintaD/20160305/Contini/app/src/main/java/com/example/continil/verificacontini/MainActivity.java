package com.example.continil.verificacontini;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {
    private List<Lista> miaLista = new ArrayList<Lista>();
    String Indirizzo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        riempilista();
        riempiListView();
        getsClick();



    }

    private void getsClick() {
        ListView lista = (ListView)findViewById(R.id.lista1);
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id){
                int val=0;
                Lista OggCliccato = miaLista.get(position);
                //String mss = "Hai cliccato" + position + "Con il nome" + OggCliccato.getNome();
                //Toast.makeText(MainActivity.this,mss,Toast.LENGTH_LONG).show();

                if (position == 0){
                    Intent intent = new Intent(MainActivity.this, Pagina.class);
                    intent.putExtra("http://w3school.com/",val);
                    startActivityForResult(intent, 42);
                    //startActivity(intent);
                }
                else if (position == 1){
                    Intent intent = new Intent(MainActivity.this, Pagina.class);
                    Indirizzo = intent.getStringExtra("https://google.it/");
                    startActivity(intent);
                }
                else{
                    Intent intent = new Intent(MainActivity.this, Pagina.class);
                    Indirizzo = intent.getStringExtra("http://openweathermap.org/");
                    startActivity(intent);
                }
            }
        });

    }

    private void riempiListView() {
        ArrayAdapter<Lista> adapter = new MioAdapter();
        ListView lista = (ListView)findViewById(R.id.lista1);
        lista.setAdapter(adapter);
    }

    private void riempilista() {

        miaLista.add(new Lista("W3C", R.drawable.android_browser_256, "http://w3school.com/"));
        miaLista.add(new Lista("Google", R.drawable.android_bluetooth_256, "https://google.it/"));
        miaLista.add(new Lista("Meteo", R.drawable.openweathermap_orange_website, "http://openweathermap.org/"));

    }



    private class MioAdapter extends ArrayAdapter<Lista>{

        public MioAdapter(){
            super(MainActivity.this, R.layout.da_lista, miaLista);
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            View oggetto = convertView;
            if (oggetto == null){
                oggetto = getLayoutInflater().inflate(R.layout.da_lista, parent, false);
            }
            Lista nome_corrente = miaLista.get(position);

            ImageView img = (ImageView)oggetto.findViewById(R.id.icona);
            img.setImageResource(nome_corrente.getIDicona());

            TextView Nome = (TextView)oggetto.findViewById(R.id.TV_Nome);
            Nome.setText(nome_corrente.getNome());

            TextView Ind = (TextView)oggetto.findViewById(R.id.TV_Indirizzo);
            Ind.setText(nome_corrente.getIndirizzo());

            return oggetto;
        }
    }
}