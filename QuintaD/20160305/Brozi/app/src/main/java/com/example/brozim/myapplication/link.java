package com.example.brozim.myapplication;

/**
 * Created by brozim on 05/03/2016.
 */
public class link {


    int IDicona;
    String Nome, Link;

    public link(int IDicona, String nome, String link) {
        this.IDicona = IDicona;
        Nome = nome;
        Link = link;
    }

    public int getIDicona() {
        return IDicona;
    }

    public void setIDicona(int IDicona) {
        this.IDicona = IDicona;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    public String getLink() {
        return Link;
    }

    public void setLink(String link) {
        Link = link;
    }
}


