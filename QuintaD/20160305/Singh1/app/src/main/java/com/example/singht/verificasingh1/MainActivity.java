package com.example.singht.verificasingh1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {
    private List<primalista> mia_lista  = new ArrayList<primalista>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        riempilista();
        riempiListView();
        gestioneCLick();
    }

    private void gestioneCLick() {

        ListView lista = (ListView)findViewById(R.id.listView);
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                primalista OggettoCliccato = mia_lista.get(position);
                String mss = "Hai cliccato " + position
                        + " Con il nome " + OggettoCliccato.getIcona1();
                Toast.makeText(MainActivity.this, mss, Toast.LENGTH_LONG).show();

            }
        });


    }

    private void riempiListView() {

        ArrayAdapter<primalista> adapter = new MioAdapter();
        ListView lista = (ListView)findViewById(R.id.listView);
        lista.setAdapter(adapter);
    }

    private void riempilista() {

        mia_lista.add(new primalista("nome1", "logo1", "nome2", R.drawable.android_browser_256));


    }

    private class MioAdapter extends ArrayAdapter<primalista> {

        public MioAdapter() {
            super(MainActivity.this, R.layout.da_lista, mia_lista);
            // TODO Auto-generated constructor stub
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            View oggetto = convertView;
            if (oggetto == null){
                oggetto = getLayoutInflater().inflate(R.layout.da_lista, parent, false);
            }
            primalista nome_corrente = mia_lista.get(position);

            ImageView img = (ImageView)oggetto.findViewById(R.id.icona);
            img.setImageResource(nome_corrente.getIcona1());


            return oggetto;


        }


    }

}