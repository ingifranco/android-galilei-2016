package com.example.lalb.verifica_lal;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;



/**
 * Created by lalb on 11/03/2016.
 */
public web extends MainActivity {

    WebView myWebV;

   @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web);

        Intent intent = getIntent();
        String Link = intent.getStringExtra("Url");
        myWebV = (WebView) findViewById(R.id.web1);
        //Abilito javascript
        WebSettings WebS = myWebV.getSettings();
        WebS.setJavaScriptEnabled(true);
        myWebV.loadUrl(Link);
    }
}
