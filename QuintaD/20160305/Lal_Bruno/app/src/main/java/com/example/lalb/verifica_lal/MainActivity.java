package com.example.lalb.verifica_lal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<figlio> mia_lista  = new ArrayList<figlio>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        riempilista();
        riempiListView();
        gestioneCLick();
    }

    private void gestioneCLick() {

        ListView lista = (ListView)findViewById(R.id.listView);
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                figlio OggettoCliccato = mia_lista.get(position);
                String mss = "Hai cliccato " + OggettoCliccato.getLogo1();
                String url = OggettoCliccato.getNome1();
                Intent intent = new Intent(MainActivity.this,web.class);
                intent.putExtra("Url",url);
                startActivity(intent);

            }
        });


    }

    private void riempiListView() {

        ArrayAdapter<figlio> adapter = new MioAdapter();
        ListView lista = (ListView)findViewById(R.id.listView);
        lista.setAdapter(adapter);
    }

    private void riempilista() {

        mia_lista.add(new figlio("www.w3schools.com","W3C",R.drawable.android_browser_256));
        mia_lista.add(new figlio("www.google.it","GOOGLE",R.drawable.android_bluetooth_256));
        mia_lista.add(new figlio("www.openweathermap.org","METEO",R.drawable.openweathermap_orange_website));


    }

    private class MioAdapter extends ArrayAdapter<figlio> {

        public MioAdapter() {
            super(MainActivity.this, R.layout.da_lista, mia_lista);
            // TODO Auto-generated constructor stub
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            View oggetto = convertView;
            if (oggetto == null){
                oggetto = getLayoutInflater().inflate(R.layout.da_lista, parent, false);
            }
            figlio nome_corrente = mia_lista.get(position);

            ImageView img = (ImageView)oggetto.findViewById(R.id.icona);
            img.setImageResource(nome_corrente.getIcona1());

            TextView Sitename = (TextView)oggetto.findViewById(R.id.text1);
            Sitename.setText(nome_corrente.getNome1());

            TextView Url = (TextView)oggetto.findViewById(R.id.text1_1);
            Url.setText(nome_corrente.getLogo1());


            return oggetto;


        }


    }

}
