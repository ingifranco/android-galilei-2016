package com.example.manghia.verifica_uno_manghi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageButton;

/**
 * Created by manghia on 04/03/2016.
 */
public class Visualizza_Sito extends AppCompatActivity {
    WebView WebV;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        WebV = (WebView)findViewById(R.id.webV1);
        ImageButton Home = (ImageButton)findViewById(R.id.imgbtn);

        Intent I = getIntent();
        String Link = I.getStringExtra("Link");
        WebSettings WebS = WebV.getSettings();
        WebS.setJavaScriptEnabled(true);
        WebV.loadUrl(Link);
        Home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
