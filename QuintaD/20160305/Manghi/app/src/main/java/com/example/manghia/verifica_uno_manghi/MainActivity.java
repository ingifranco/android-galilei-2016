package com.example.manghia.verifica_uno_manghi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<ListaImmagini> miaLista = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        riempilista();
        riempiListView();
        elemento_lista();
    }

    private class MioAdapter extends ArrayAdapter<ListaImmagini> {
        public MioAdapter() { super(MainActivity.this, R.layout.da_lista,miaLista);}
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View oggetto = convertView;
            if(oggetto == null){
                oggetto = getLayoutInflater().inflate(R.layout.da_lista, parent, false);
            }
            ListaImmagini riga = miaLista.get(position);

            ImageView Image = (ImageView)oggetto.findViewById(R.id.imageV1);
            Image.setImageResource(riga.getImage());

            TextView Link = (TextView)oggetto.findViewById(R.id.textlink);
            Link.setText(riga.getLink());

            TextView Sito = (TextView)oggetto.findViewById(R.id.textsito);
            Sito.setText(riga.getSito());

            return  oggetto;
        }
    }

    private void riempiListView(){
        ListView lista = (ListView)findViewById(R.id.listV1);
        ArrayAdapter<ListaImmagini> adapter = new MioAdapter();
        lista.setAdapter(adapter);
    }

    private void elemento_lista(){
        ListView lista = (ListView)findViewById(R.id.listV1);
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3){
                    ListaImmagini Elemento = miaLista.get(position);
                    String Link = Elemento.getLink();
                    Intent I = new Intent (MainActivity.this, Visualizza_Sito.class);
                    I.putExtra("Link", Link);
                    startActivity(I);
            }
           });
        }

    private void riempilista(){
        miaLista.add(new ListaImmagini("http://www.w3schools.com/", "W3C", R.drawable.android_browser_256));
        miaLista.add(new ListaImmagini("http://www.google.it/", "Google", R.drawable.android_bluetooth_256));
        miaLista.add(new ListaImmagini("http://openweathermap.org/", "Meteo", R.drawable.openweathermap_orange_website));
    }
}
