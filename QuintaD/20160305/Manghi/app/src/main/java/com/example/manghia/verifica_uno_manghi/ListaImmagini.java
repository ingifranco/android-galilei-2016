package com.example.manghia.verifica_uno_manghi;

/**
 * Created by manghia on 04/03/2016.
 */
public class ListaImmagini {
    String Link, Sito;
    int Image;

    public String getLink() {return Link;}
    public int getImage() {return Image;}
    public String getSito() {return Sito;}
    public void setLink(String link) {Link = link;}
    public void setImage(int image) {Image = image;}
    public void setSito(String sito) {Sito = sito;}
    public ListaImmagini(String testo1, String testo2, int img)
    {
        super();
        Link = testo1;
        Sito = testo2;
        Image = img;
    }

}
