package com.example.admin.dodonu_0403;

/**
 * Created by Admin on 04/03/2016.
 */
public class Link {
    int IDicona;
    String Nome, Link;

    public Link(int IDicona, String nome, String link) {
        this.IDicona = IDicona;
        Nome = nome;
        Link = link;
    }

    public int getIDicona() {
        return IDicona;
    }

    public void setIDicona(int IDicona) {
        this.IDicona = IDicona;
    }

    public String getLink() {
        return Link;
    }

    public void setLink(String link) {
        Link = link;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }
}
