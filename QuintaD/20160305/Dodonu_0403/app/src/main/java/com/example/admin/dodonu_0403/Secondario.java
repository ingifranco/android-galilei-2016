package com.example.admin.dodonu_0403;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Admin on 04/03/2016.
 */
public class Secondario extends Activity {

    private String val;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.secondario);


        Intent intent = getIntent(); // prendo i parametri passati prima
        String Link = intent.getStringExtra("valore");
        WebView myWebV = (WebView) findViewById(R.id.myWebV);
        //Abilito javascript
        WebSettings WebS = myWebV.getSettings();
        WebS.setJavaScriptEnabled(true);
        myWebV.loadUrl(Link);

        ImageView Indietro = (ImageView)findViewById(R.id.imgBack);

        Indietro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
