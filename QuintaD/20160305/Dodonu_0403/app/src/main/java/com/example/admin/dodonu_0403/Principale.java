package com.example.admin.dodonu_0403;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
<<<<<<< HEAD
=======
import android.widget.Toast;
>>>>>>> f095f6f7132923b3e31a7a58c7590b3db30c13b7

import com.melnykov.fab.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class Principale extends Activity {
    ListView lista;
    int ID=-1;
    private List<Link> miaLista = new ArrayList<Link>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principale);
        lista = (ListView)findViewById(R.id.lista1);
        registerForContextMenu(lista);
        riempilista();
        riempiListView();
        gestioneClick();
        aggAllaLista();
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        if (v.getId() == R.id.lista1) {
            ListView lv = (ListView) v;
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo)menuInfo;
            Link obj = (Link) lv.getItemAtPosition(acmi.position);

            menu.add("Elimina");
            menu.add("Modifica");
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int icona;
        String Nome, Link;

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        if(item.getTitle()=="Elimina"){
            miaLista.remove(info.position);
            riempiListView();
        }
        else if(item.getTitle()=="Modifica"){
            icona = miaLista.get(info.position).getIDicona();
            Nome = miaLista.get(info.position).getNome();
            Link = miaLista.get(info.position).getLink();
            Intent intent = new Intent(Principale.this, NuovoDato.class);
            intent.putExtra("IconaM", icona);
            intent.putExtra("NomeM", Nome);
            intent.putExtra("LinkM", Link);
            //intent.putExtra("ID", info.position);
            ID=info.position;
            startActivityForResult(intent, 2);
        }else{
            return false;
        }
        return true;
    }

    private void aggAllaLista() {
        FloatingActionButton btn = (FloatingActionButton)findViewById(R.id.fab);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Principale.this, NuovoDato.class);
                startActivityForResult(intent, 1);
            }
        });

    }

    private void gestioneClick() {
        // TODO Auto-generated method stub
        //ListView lista = (ListView)findViewById(R.id.lista1);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View viewClicked,
                                    int position, long id) {
                Link OggettoCliccato = miaLista.get(position);
                Intent intent = new Intent(Principale.this, Secondario.class);
                intent.putExtra("valore", OggettoCliccato.getLink());
                startActivity(intent);

            }
        });
<<<<<<< HEAD
    }


=======

    }
>>>>>>> f095f6f7132923b3e31a7a58c7590b3db30c13b7
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_CANCELED) {
            return;
        }
        else {
            int icona1;
            String Nome_r,Link_r;
            switch (requestCode) {
                case 1:
                    icona1 = data.getIntExtra("Icona", 0);
                    Nome_r = data.getStringExtra("Nome");
                    Link_r = data.getStringExtra("Link");
                    miaLista.add(new Link(icona1, Nome_r, Link_r));
                    riempiListView();
                    break;
                case 2:
                    //ID = data.getIntExtra("ID",0);
                    icona1 = data.getIntExtra("Icona", 0);
                    Nome_r = data.getStringExtra("Nome");
                    Link_r = data.getStringExtra("Link");
                    miaLista.get(ID).setIDicona(icona1);
                    miaLista.get(ID).setNome(Nome_r);
                    miaLista.get(ID).setLink(Link_r);
                    riempiListView();
                   // ID=-1;
                    break;
            }
        }
    }

    private void riempiListView() {

        ArrayAdapter<Link> adapter = new MioAdapter();
        //ListView lista = (ListView)findViewById(R.id.lista1);
        lista.setAdapter(adapter);
    }

    private void riempilista() {
        miaLista.add(new Link( R.drawable.android_browser_256, "W3C", "http://www.w3schools.com/"));
        miaLista.add(new Link( R.drawable.android_bluetooth_256, "Google", "https://www.google.it/"));
        miaLista.add(new Link( R.drawable.openweathermap_orange_website, "Meteo", "http://www.openweathermap.org/"));
    }

    private class MioAdapter extends ArrayAdapter<Link>{

        public MioAdapter() {
            super(Principale.this, R.layout.da_lista, miaLista);
            // TODO Auto-generated constructor stub
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            View oggetto = convertView;
            if (oggetto == null) {
                oggetto = getLayoutInflater().inflate(R.layout.da_lista, parent, false);
            }
            Link nome_corrente = miaLista.get(position);

            ImageView img = (ImageView)oggetto.findViewById(R.id.icona);
            img.setImageResource(nome_corrente.getIDicona());

            TextView Nome = (TextView)oggetto.findViewById(R.id.txtNome);
            Nome.setText(nome_corrente.getNome());

            TextView Cognome = (TextView)oggetto.findViewById(R.id.txtLink);
            Cognome.setText(nome_corrente.getLink());

            return oggetto;


        }


    }

}
