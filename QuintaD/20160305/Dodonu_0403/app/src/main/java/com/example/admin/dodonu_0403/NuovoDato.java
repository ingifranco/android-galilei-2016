package com.example.admin.dodonu_0403;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Admin on 04/03/2016.
 */
public class NuovoDato extends Activity {

    int icona1=0;
    String Nome,Link;
    EditText edN,edL;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nuovodato);
        edN = (EditText)findViewById(R.id.edNome);
        edL = (EditText)findViewById(R.id.edLink);

        Intent intent = getIntent();
        //ID = intent.getIntExtra("ID", -1);
        icona1 = intent.getIntExtra("IconaM", 0);
        Nome = intent.getStringExtra("NomeM");
        Link = intent.getStringExtra("LinkM");
        edN.setText(Nome);
        edL.setText(Link);
        gestclicc();

    }

    private void gestclicc() {
        Button Salva = (Button)findViewById(R.id.btnOK);
        Button Canc = (Button)findViewById(R.id.btnCanc);
        Salva.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                Nome = edN.getText().toString();
                Link = edL.getText().toString();
                if((Nome.equals(""))||(Link.equals(""))||(icona1 == 0)){
                    Toast.makeText(NuovoDato.this,"Non hai compilato tutti i campi",Toast.LENGTH_SHORT ).show();
                }
                else
                {
                    Intent intent = new Intent();
                    //intent.putExtra("ID",ID);
                    intent.putExtra("Nome", Nome);
                    intent.putExtra("Link", Link);
                    intent.putExtra("Icona", icona1);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }
            }
        });

        Canc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                setResult(Activity.RESULT_CANCELED, intent);
                finish();
            }
        });
    }

    public void onShow(View v){
        TextView txtI = (TextView)findViewById(R.id.txtI);
        switch (v.getId()){

            case R.id.im1:
                icona1=R.drawable.android_1;
                txtI.setText("im1");
                break;
            case R.id.im2:
                icona1 = R.drawable.android_2;
                txtI.setText("im2");
                break;
            case R.id.im3:
                icona1 = R.drawable.android_3;
                txtI.setText("im3");
                break;
            case R.id.im4:
                icona1 = R.drawable.android_4;
                txtI.setText("im4");
                break;
            case R.id.im5:
                icona1 = R.drawable.android_angry_birds_128;
                txtI.setText("im5");
                break;
            case R.id.im6:
                icona1 = R.drawable.android_bluetooth_256;
                txtI.setText("im6");
                break;
            case R.id.im7:
                icona1 = R.drawable.android_browser_256;
                txtI.setText("im7");
                break;
            case R.id.im8:
                icona1 = R.drawable.android_newsweather_256;
                txtI.setText("im8");
                break;
            case R.id.im9:
                icona1 = R.drawable.android_5;
                txtI.setText("im9");
                break;
            case R.id.im10:
                icona1 = R.drawable.android_6;
                txtI.setText("im10");
                break;


        }
    }

}
