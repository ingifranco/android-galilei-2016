package com.example.admin.verifica_1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<Link_class> mLista = new ArrayList<Link_class>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        riempilista();
        riempiListView();
        gestioneclick();

    }

    private void gestioneclick() {
        ListView lista = (ListView)findViewById(R.id.lista1);
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Link_class OggCliccato = mLista.get(position); // variile oggiCliccto  di tipo link class posizione di mLisa
                String mss = "Hai cliccato " + OggCliccato.getNome();
                Toast.makeText(MainActivity.this, mss, Toast.LENGTH_SHORT).show(); //messaggio di corta durta mand a video mss e il perido
                                                                                    // di tempo:Lenght_short
            }
        });



    }

    private void riempiListView() {
        ArrayAdapter<Link_class> adapter = new MioAdapter();
        ListView lista = (ListView)findViewById(R.id.lista1);
        lista.setAdapter(adapter);
    }

    private void riempilista() {
        mLista.add(new Link_class(R.drawable.android_browser_256, "W3C", "http://wwww.w3schols.com/"));
        mLista.add(new Link_class(R.drawable.android_bluetooth_256, "Google", "http://wwww.google.it/"));
        mLista.add(new Link_class(R.drawable.openweathermap_orange_website, "meteo.it", "http://http://www.meteo.it//"));

    }



    private class MioAdapter extends ArrayAdapter<Link_class>{

        public MioAdapter(){
            super(MainActivity.this, R.layout.da_lista, mLista);
        }
        public View getView(int position, View convertView, ViewGroup parent){
            View oggetto = convertView;
            if (oggetto == null){
                oggetto = getLayoutInflater().inflate(R.layout.da_lista, parent, false);
            }
            Link_class nome_corrente = mLista.get(position);

            ImageView img = (ImageView)oggetto.findViewById(R.id.icona);
            img.setImageResource(nome_corrente.geticona());

            TextView Nome = (TextView)oggetto.findViewById(R.id.txtNome);
            Nome.setText(mLista.get(position).getNome());

            TextView url = (TextView)oggetto.findViewById(R.id.txturl);
            url.setText(nome_corrente.geturl());

            return oggetto;

        }


    }

}