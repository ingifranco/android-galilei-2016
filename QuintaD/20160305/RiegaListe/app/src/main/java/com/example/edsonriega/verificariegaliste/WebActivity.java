package com.example.edsonriega.verificariegaliste;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;

public class WebActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        ImageView imageView = (ImageView) findViewById(R.id.imageView2);
        WebView myWeb = (WebView) findViewById(R.id.webView);
        String URL = getIntent().getStringExtra("URL");

        imageView.setImageResource(R.drawable.android_angry_birds_128);
        WebSettings webs = myWeb.getSettings();
        webs.setJavaScriptEnabled(true);
        myWeb.loadUrl(URL);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WebActivity.this,MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
