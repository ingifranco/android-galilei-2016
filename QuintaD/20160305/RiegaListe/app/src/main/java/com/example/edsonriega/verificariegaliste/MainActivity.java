package com.example.edsonriega.verificariegaliste;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<Sito> Siti = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ListView listView = (ListView) findViewById(R.id.listView);
        Siti.add(new Sito("W3C", R.drawable.android_browser_256, "http://www.w3schools.com/"));
        Siti.add(new Sito("Google",R.drawable.android_bluetooth_256, "https://google.it/"));
        Siti.add(new Sito("Meteo",R.drawable.openweathermap_orange_website, "http://www.openweathermap.org/"));
        Siti.add(new Sito("Milan",R.drawable.logomilan,"http://www.acmilan.com/it"));
        SuperAdapter adapter = new SuperAdapter(this, R.layout.row, Siti);
        listView.setAdapter(adapter);
        listaclick(listView);
    }

    private void listaclick(ListView listView){
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this,WebActivity.class);
                Sito s = Siti.get(position);
                intent.putExtra("URL",s.getUrl());
                startActivity(intent);
            }
        });
    }
}
