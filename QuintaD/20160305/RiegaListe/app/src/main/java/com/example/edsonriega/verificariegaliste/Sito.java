package com.example.edsonriega.verificariegaliste;

/**
 * Created by Edson Riega on 04/03/2016.
 */
public class Sito {
    String nome;
    int imageID;
    String url;

    public Sito(String nome, int imageID, String url){
        this.nome = nome;
        this.imageID = imageID;
        this.url = url;
    }

    public String getNome(){
        return nome;
    }

    public int getImageID(){
        return imageID;
    }

    public String getUrl(){
        return url;
    }
}
