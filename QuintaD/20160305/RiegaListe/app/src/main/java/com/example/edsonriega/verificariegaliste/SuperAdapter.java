package com.example.edsonriega.verificariegaliste;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;

public class SuperAdapter extends ArrayAdapter<Sito>{
    public SuperAdapter(Context context, int textViewResourcedID, ArrayList<Sito> oggetti){
        super(context,textViewResourcedID,oggetti);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        ViewHolder viewHolder;
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row,null);
            viewHolder = new ViewHolder();

            viewHolder.url = (TextView) convertView.findViewById(R.id.textView);
            viewHolder.nome = (TextView) convertView.findViewById(R.id.textView2);
            viewHolder.image = (ImageView) convertView.findViewById(R.id.imageView);

            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Sito s = getItem(position);
        viewHolder.nome.setText(s.getNome());
        viewHolder.url.setText(s.getUrl());
        viewHolder.image.setImageResource(s.getImageID());

        return convertView;
    }

    private class ViewHolder{
        public TextView nome;
        public TextView url;
        public ImageView image;
    }
}
