package com.example.bettatil.verificabettati0403;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;

/**
 * Created by bettatil on 05/03/2016.
 */
public class Principale2 {

    import android.webkit.WebSettings;
    import android.webkit.WebView;


    public class Principale2 extends AppCompatActivity {

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.principale2);

            String sito = getIntent().getStringExtra("sito");
            WebView myweb = (WebView) findViewById(R.id.webView);
            WebSettings webs = myweb.getSettings();
            webs.setJavaScriptEnabled(true);
            myweb.loadUrl(sito);
        }
}
