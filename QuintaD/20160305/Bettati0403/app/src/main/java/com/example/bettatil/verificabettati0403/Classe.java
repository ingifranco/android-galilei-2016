package com.example.bettatil.verificabettati0403;

/**
 * Created by bettatil on 05/03/2016.
 */
public class Classe {
    String sito;
    String nome;
    int img;

    public Classe(String sito, String nome, int img) {
        this.nome = sito;
        this.sito = nome;
        this.img = img;
    }

    public String get_sito() {return this.sito; }

    public String get_nome(){
        return this.nome;
    }

    public int get_img(){
        return this.img;
    }
}
