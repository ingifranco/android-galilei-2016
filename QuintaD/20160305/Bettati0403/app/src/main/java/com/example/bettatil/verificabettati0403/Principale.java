package com.example.bettatil.verificabettati0403;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebSettings;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class Principale extends AppCompatActivity {
    private List<Classe> lista = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principale);
        riempilista();
        riempiListView();
        gestioneClick();

    }


    public void riempilista() {
        lista.add(new Classe("W3C", "http://www.w3schools.com/", R.drawable.android_browser_256));
        lista.add(new Classe("Google", "http://www.google.it/", R.drawable.android_bluetooth_256));
        lista.add(new Classe("Meteo", "http://www.openweathermap.com/", R.drawable.openweathermap_orange_website));
    }

    private void riempiListView() {
        ArrayAdapter<Classe> adapter = new MioAdapter();
        ListView lista = (ListView)findViewById(R.id.lista1);
        lista.setAdapter(adapter);
    }

    private void gestioneClick() {
        final ListView listaw = (ListView)findViewById(R.id.lista1);
        listaw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View viewClicked,int position, long id) {
                Classe OggettoCliccato = lista.get(position);
                Intent k = new Intent(Principale.this,Principale2.class);
                k.putExtra("sito",OggettoCliccato.get_sito());
                startActivity(k);
            }
        });

    }

    private class MioAdapter extends ArrayAdapter<Classe>{

        public MioAdapter() {
            super(Principale.this, R.layout.da_lista,lista);
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            View oggetto = convertView;
            if (oggetto == null){
                oggetto = getLayoutInflater().inflate(R.layout.da_lista, parent, false);
            }
            Classe sito_usato = lista.get(position);

            ImageView img = (ImageView)oggetto.findViewById(R.id.imageView);
            img.setImageResource(sito_usato.img);

            TextView sito = (TextView)oggetto.findViewById(R.id.textView);
            sito.setText(sito_usato.get_sito());

            TextView nome= (TextView)oggetto.findViewById(R.id.textView2);
            nome.setText(sito_usato.get_nome());

            return oggetto;

        }
    }
}

}
