package com.example.cavalcani.cavalca;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;



import java.util.ArrayList;
import java.util.List;



public class MainActivity extends AppCompatActivity {
    private List<Link> miaLista = new ArrayList<Link>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        riempilista();
        riempiListView();
        gestioneclick();

    }

    private void riempilista() {
        miaLista.add(new Link(R.drawable.android_browser_256, "W3C", "http://wwww.w3schols.com/"));
        miaLista.add(new Link(R.drawable.android_bluetooth_256, "Google", "http://wwww.google.it/"));
        miaLista.add(new Link(R.drawable.openweathermap_orange_website, "Meteo", "http://wwww.openwethermap.org/"));

    }

    private void riempiListView() {
        ArrayAdapter<Link> adapter = new MioAdapter();
        ListView lista = (ListView) findViewById(R.id.lista1);
        lista.setAdapter(adapter);
    }

    private void gestioneclick() {
        ListView lista = (ListView) findViewById(R.id.lista1);
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Link OggCliccato = miaLista.get(position);
                String mss = "Hai cliccato " + OggCliccato.getNome();
                Toast.makeText(MainActivity.this, mss, Toast.LENGTH_SHORT).show();
            }
        });
    }
        private class MioAdapter extends ArrayAdapter<Link> {

            public MioAdapter() {
                super(MainActivity.this, R.layout.da_lista, miaLista);
            }

            public View getView(int position, View convertView, ViewGroup parent) {
                View oggetto = convertView;
                if (oggetto == null) {
                    oggetto = getLayoutInflater().inflate(R.layout.da_lista, parent, false);
                }
                Link nome_corrente = miaLista.get(position);

                ImageView img = (ImageView) oggetto.findViewById(R.id.icona);
                img.setImageResource(nome_corrente.getIDicona());

                TextView Nome = (TextView) oggetto.findViewById(R.id.txtNome);
                Nome.setText(miaLista.get(position).getNome());

                TextView Link = (TextView) oggetto.findViewById(R.id.txtLink);
                Link.setText(nome_corrente.getLink());

                return oggetto;

            }
        }
    }

