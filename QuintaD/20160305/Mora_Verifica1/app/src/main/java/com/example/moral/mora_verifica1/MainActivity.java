package com.example.moral.mora_verifica1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<ListaImmagini> miaLista = new ArrayList<ListaImmagini>();
     @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
         riempilista();
         riempiListView();
         gestioneclik();
    }

    private void riempilista() {
        miaLista.add(new ListaImmagini("http://www.w3schools.com/", "W3C", R.drawable.android_browser_256));
        miaLista.add(new ListaImmagini("http://www.google.it/", "Google", R.drawable.android_bluetooth_256));
        miaLista.add(new ListaImmagini("http://openweathermad.ora/", "Meteo", R.drawable.openweathermap_orange_website));

    }

    private void riempiListView() {
        // TODO Auto-generated method stub
        ArrayAdapter<ListaImmagini> adapter = new MioAdapter();
        ListView lista = (ListView)findViewById(R.id.List1);
        lista.setAdapter(adapter);

    }

    private void gestioneclik()  {

        ListView lista = (ListView)findViewById(R.id.List1);
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ListaImmagini OggCliccato = miaLista.get(position);
                String Url =  OggCliccato.getTesto2();

                Intent intent = new Intent(MainActivity.this,SecondActivity.class);
                intent.putExtra("url",Url);
                startActivity(intent);
            }
        });





    }

    private class MioAdapter extends ArrayAdapter<ListaImmagini> {
        public MioAdapter() {
            super(MainActivity.this, R.layout.da_lista,miaLista);
        }
        @Override
        public View getView(int position,View convertView, ViewGroup parent){
            View oggetto = convertView;
            if(oggetto == null){
                oggetto = getLayoutInflater().inflate(R.layout.da_lista, parent, false);
            }
            ListaImmagini riga = miaLista.get(position);

            TextView testo = (TextView)oggetto.findViewById(R.id.txt1);
            testo.setText(riga.getTesto());

            TextView web = (TextView)oggetto.findViewById(R.id.txtweb);
            web.setText(riga.getTesto2());

            ImageView img1 = (ImageView)oggetto.findViewById(R.id.Image1);
            img1.setImageResource(riga.getImg1());

            return oggetto;
        }
    }
}
