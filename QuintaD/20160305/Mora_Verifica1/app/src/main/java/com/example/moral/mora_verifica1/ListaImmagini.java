package com.example.moral.mora_verifica1;

/**
 * Created by moral on 04/03/2016.
 */
public class ListaImmagini {
    String Testo,Testo2;

    int Img1;

    public ListaImmagini(String testo, String testo2 , int img1) {
        super();
        Testo = testo;
        Testo2 = testo2;
        Img1 = img1;

    }

    public String getTesto2() {
        return Testo2;
    }

    public int getImg1() {
        return Img1;
    }

    public void setTesto(String testo) {
        Testo = testo;
    }

    public void setImg1(int img1) {Img1 = img1;}

    public void setTesto2(String testo2) {Testo2 = testo2; }

    public String getTesto() {
        return Testo;
    }
}
