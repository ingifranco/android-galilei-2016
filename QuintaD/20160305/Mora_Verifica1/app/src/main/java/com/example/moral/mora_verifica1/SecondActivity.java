package com.example.moral.mora_verifica1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageButton;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Intent intent = getIntent();
        String Indirizzo = intent.getStringExtra("Url");
        WebView mywebV = (WebView) findViewById(R.id.webView1);
        WebSettings webS = mywebV.getSettings();
        webS.setJavaScriptEnabled(true);
        mywebV.loadUrl(Indirizzo);
        ImageButton bt = (ImageButton)findViewById(R.id.btn1);
       // bt.setOnClickListener();
    }
}
