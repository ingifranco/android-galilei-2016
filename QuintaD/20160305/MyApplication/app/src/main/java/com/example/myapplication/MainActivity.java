package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.view.LayoutInflater;
import android.content.Context;
import java.util.ArrayList;
import java.util.List;



public class MainActivity extends AppCompatActivity {


    ListView listview;
    private List<Element> miaLista = new ArrayList<Element>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listview = (ListView)findViewById(R.id.listView);

        miaLista.add(new Element("http://www.w3c.it/it/1/ufficio-italiano-w3c.html", "W3C", R.drawable.android_browser_256));
        miaLista.add(new Element("http://www.google.com", "Google", R.drawable.android_bluetooth_256));
        miaLista.add(new Element("url3", "Meteo", R.drawable.openweathermap_orange_website));
        ArrayAdapter<Element> adapter = new MioAdapter(this, R.layout.list_element, miaLista);
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Element cliccato = miaLista.get(position);
                Intent intent = new Intent(MainActivity.this, Activity2.class);
                intent.putExtra("URL", cliccato.getUrl());
                startActivity(intent);

            }
        });
    }


    private class MioAdapter extends ArrayAdapter<Element>{

        public MioAdapter(Context context, int textViewResourceId, List<Element> lista)
        {
            super(context, textViewResourceId, lista);
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_element, null);
            ImageView img = (ImageView)convertView.findViewById(R.id.imageView2);
            img.setImageResource(miaLista.get(position).getImage());

            TextView url = (TextView)convertView.findViewById(R.id.textView);
            url.setText(miaLista.get(position).getUrl());

            TextView titolo = (TextView)convertView.findViewById(R.id.textView2);
            titolo.setText(miaLista.get(position).getTitolo());

            return convertView;

        }
    }


}
