package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.Toast;
import android.webkit.WebViewClient;


public class Activity2 extends AppCompatActivity {

    WebView browser;
    ImageView image;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);


        browser = (WebView) findViewById(R.id.webView);
        image = (ImageView)findViewById(R.id.imageView3);

        image.setOnClickListener(new View.OnClickListener() {
           @Override
              public void onClick(View v) {
               Intent intent = new Intent(Activity2.this, MainActivity.class);
               startActivity(intent);
              }
        });
    }

    protected void onResume()
    {
        super.onResume();
        Intent intent = getIntent();
        url = intent.getStringExtra("URL");
        browser.getSettings().setJavaScriptEnabled(true);
        browser.setWebViewClient(new myWebViewClient());
        browser.loadUrl(url);
        Toast.makeText(getApplicationContext(),
                url, Toast.LENGTH_LONG).show();
    }


    public class myWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
