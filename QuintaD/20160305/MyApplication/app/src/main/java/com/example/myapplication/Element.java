package com.example.myapplication;

/**
 * Created by marco on 04/03/2016.
 */
public class Element {

    String url, titolo;
    int image;

    public Element(String url, String titolo, int image) {
        this.url = url;
        this.titolo = titolo;
        this.image = image;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitolo() {
        return titolo;
    }

    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }




}
