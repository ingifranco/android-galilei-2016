package com.example.iacolinas.iacolinaverifica;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class PRINCIPALE extends AppCompatActivity {

    private List<Classe> miaLista = new ArrayList<Classe>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principale);

        riempilista();
        riempiListView();
        gestioneclick();


    }
    private void gestioneclick() {
        ListView lista = (ListView) findViewById(R.id.lista1);
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Classe OggCliccato = miaLista.get(position);

                Intent intent = new Intent(PRINCIPALE.this, Internet.class);
                intent.putExtra("URL", OggCliccato.getTesto1());
                startActivity(intent);
            }
        });
    }

    private void riempiListView() {
        ArrayAdapter<Classe> adapter = new MioAdapter();
        ListView lista = (ListView) findViewById(R.id.lista1);
        lista.setAdapter(adapter);

    }

    private void riempilista() {
        miaLista.add(new Classe("http://www.w3schools.com/", R.drawable.android_browser_256, "W3C"));
        miaLista.add(new Classe("http://www.google.com/", R.drawable.android_bluetooth_256, "GOOGLE"));
        miaLista.add(new Classe("http://www.ilmeteo.com/", R.drawable.openweathermap_orange_website, "METEO"));
    }

        private class MioAdapter extends ArrayAdapter<Classe> {

            public MioAdapter() {
                super(PRINCIPALE.this, R.layout.secondo, miaLista);
            }

            public View getView(int position, View convertView, ViewGroup parent) {
                View oggetto = convertView;
                if (oggetto == null) {
                    oggetto = getLayoutInflater().inflate(R.layout.secondo, parent, false);//FINE LISTA
                }
                Classe nome_corrente = miaLista.get(position);

                ImageView img = (ImageView) oggetto.findViewById(R.id.Icona1);
                img.setImageResource(nome_corrente.getIDicona());

                TextView Link = (TextView) oggetto.findViewById(R.id.txt1);
                Link.setText(nome_corrente.getTesto1());

                TextView No = (TextView) oggetto.findViewById(R.id.tx2);
                No.setText(nome_corrente.getTesto2());

                return oggetto;

            }


        }
    }

