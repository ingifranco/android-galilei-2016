package com.example.iacolinas.iacolinaverifica;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;

/**
 * Created by Admin on 04/03/2016.
 */
public class Internet  extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        String Link;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.terzo);

        Intent intent = getIntent(); // prendo i parametri passati prima
        Link = intent.getStringExtra("URL");

        WebView myWebV = (WebView)findViewById(R.id.myWebV);
        WebSettings WebS = myWebV.getSettings();
        WebS.setJavaScriptEnabled(true); // attivo il javascript
        myWebV.loadUrl(Link);

        ImageView Indietro = (ImageView)findViewById(R.id.Bback);

        Indietro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


}





