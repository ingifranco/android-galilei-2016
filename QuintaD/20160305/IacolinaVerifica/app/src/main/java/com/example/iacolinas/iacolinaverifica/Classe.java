package com.example.iacolinas.iacolinaverifica;

/**
 * Created by iacolinas on 04/03/2016.
 */
public class Classe {

            String Testo1,Testo2;
            int IDicona;

    public Classe(String testo1, int IDicona, String testo2) {
        Testo1 = testo1;
        this.IDicona = IDicona;
        Testo2 = testo2;
    }

    public String getTesto1() {
        return Testo1;
    }

    public void setTesto1(String testo1) {
        Testo1 = testo1;
    }

    public int getIDicona() {
        return IDicona;
    }

    public void setIDicona(int IDicona) {
        this.IDicona = IDicona;
    }

    public String getTesto2() {
        return Testo2;
    }

    public void setTesto2(String testo2) {
        Testo2 = testo2;
    }




}
