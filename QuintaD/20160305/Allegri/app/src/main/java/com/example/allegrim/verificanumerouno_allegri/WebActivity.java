package com.example.allegrim.verificanumerouno_allegri;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageButton;

public class WebActivity extends AppCompatActivity {

    WebView myWebV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        Intent intent = getIntent();
        String Link = intent.getStringExtra("Url");
        myWebV = (WebView) findViewById(R.id.my_web_View);
        //Abilito javascript
        WebSettings WebS = myWebV.getSettings();
        WebS.setJavaScriptEnabled(true);
        myWebV.loadUrl(Link);

        ImageButton AngryBird = (ImageButton)findViewById(R.id.imageButton);
        AngryBird.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
