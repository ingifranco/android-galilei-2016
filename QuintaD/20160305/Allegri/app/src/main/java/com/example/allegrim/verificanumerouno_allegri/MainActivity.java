package com.example.allegrim.verificanumerouno_allegri;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<SiteClass> miaLista = new ArrayList<SiteClass>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        riempilista();
        riempiListView();
        gestioneClick();
    }

    private void gestioneClick() {
        // TODO Auto-generated method stub
        ListView lista = (ListView)findViewById(R.id.SitelistView);
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View viewClicked,
                                    int position, long id) {
                SiteClass OggettoCliccato = miaLista.get(position);
                String url = OggettoCliccato.getUrl();

                Intent intent = new Intent(MainActivity.this,WebActivity.class);
                intent.putExtra("Url",url);
                startActivity(intent);
            }
        });

    }

    private void riempiListView() {
        // TODO Auto-generated method stub
        ArrayAdapter<SiteClass> adapter = new MioAdapter();
        ListView lista = (ListView)findViewById(R.id.SitelistView);
        lista.setAdapter(adapter);
    }

    private void riempilista() {
        miaLista.add(new SiteClass("http://www.w3schools.com/","W3C" ,R.drawable.android_browser_256));
        miaLista.add(new SiteClass("http://www.google.it/", "Google",R.drawable.android_bluetooth_256));
        miaLista.add(new SiteClass("http://openweathermap.org/","Meteo" ,R.drawable.openweathermap_orange_website));
    }

    private class MioAdapter extends ArrayAdapter<SiteClass>{
        public MioAdapter() {
            super(MainActivity.this, R.layout.da_lista,miaLista);
        }
        @Override
        public View getView(int position,View convertView, ViewGroup parent){
            View oggetto = convertView;
            if(oggetto == null){
                oggetto = getLayoutInflater().inflate(R.layout.da_lista, parent, false);
            }
            SiteClass riga = miaLista.get(position);

            ImageView img1 = (ImageView)oggetto.findViewById(R.id.imageView);
            img1.setImageResource(riga.getImg1());

            TextView Sitename = (TextView)oggetto.findViewById(R.id.textView2);
            Sitename.setText(riga.getSitename());

            TextView Url = (TextView)oggetto.findViewById(R.id.textView);
            Url.setText(riga.getUrl());

            return oggetto;
        }
    }
}
