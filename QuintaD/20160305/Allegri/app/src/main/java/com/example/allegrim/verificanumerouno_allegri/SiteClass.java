package com.example.allegrim.verificanumerouno_allegri;

/**
 * Created by allegrim on 04/03/2016.
 */
public class SiteClass {
    String Url;
    String Sitename;
    int Img1;

    public int getImg1() {return Img1;}
    public void setImg1(int img1) {Img1 = img1;}
    public String getUrl() {return Url;}
    public void setUrl(String url) {Url = url;}
    public String getSitename() {return Sitename;}
    public void setSitename(String sitename) {Sitename = sitename;}

    public SiteClass(String url, String sitename, int img1) {
        Url = url;
        Sitename = sitename;
        Img1 = img1;
    }
}
