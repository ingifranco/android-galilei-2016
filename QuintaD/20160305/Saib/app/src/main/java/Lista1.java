
/**
 * Created by saibn on 05/03/2016.
 */

public class Lista1 {

    String Nome,Indirizzo;
    int IDicona;

    public Lista1(String nome, int IDicona, String indirizzo) {
        Nome = nome;
        this.IDicona = IDicona;
        Indirizzo = indirizzo;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    public int getIDicona() {
        return IDicona;
    }

    public void setIDicona(int IDicona) {
        this.IDicona = IDicona;
    }

    public String getIndirizzo() {
        return Indirizzo;
    }

    public void setIndirizzo(String indirizzo) {
        Indirizzo = indirizzo;
    }
}
