package com.example.saibn.verificaunosaib;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private List<Lista> miaLista = new ArrayList<Lista>();
    String Indirizzo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        riempilista();
        riempiListView();
        getsClick();



    }

    private void getsClick() {
        ListView lista1 = (ListView)findViewById(R.id.lista1);
        lista1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id){
                int val=0;
                Lista1 OggCliccato = miaLista.get(position);


                if (position == 0){
                    Intent intent = new Intent(MainActivity.this, Pagina.class);
                    intent.putExtra("http://w3school.com/",val);
                    startActivityForResult(intent, 42);

                }
                else if (position == 1){
                    Intent intent = new Intent(MainActivity.this, Pagina.class);
                    Indirizzo = intent.getStringExtra("https://google.it/");
                    startActivity(intent);
                }
                else{
                    Intent intent = new Intent(MainActivity.this, Pagina.class);
                    Indirizzo = intent.getStringExtra("http://openweathermap.org/");
                    startActivity(intent);
                }
            }
        });

    }

    private void riempiListView() {
        ArrayAdapter<Lista1> adapter = new MioAdapter();
        ListView Lista1 = (ListView)findViewById(R.id.Lista1);
        Lista1.setAdapter(adapter);
    }

    private void riempilista() {

        miaLista.add(new Lista1("W3C", R.drawable.android_browser_256, "http://w3school.com/"));
        miaLista.add(new Lista1("Google", R.drawable.android_bluetooth_256, "https://google.it/"));
        miaLista.add(new Lista1("Meteo", R.drawable.openweathermap_orange_website, "http://openweathermap.org/"));

    }



    private class MioAdapter extends ArrayAdapter<Lista1>{

        public MioAdapter(){
            super(MainActivity.this, R.layout.da_lista, miaLista);
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            View oggetto = convertView;
            if (oggetto == null){
                oggetto = getLayoutInflater().inflate(R.layout.da_lista, parent, false);
            }
            Lista1 nome_corrente = miaLista.get(position);

            ImageView img = (ImageView)oggetto.findViewById(R.id.img_icona);
            img.setImageResource(nome_corrente.getIDicona());

            TextView Nome = (TextView)oggetto.findViewById(R.id.Txt_Nome);
            Nome.setText(nome_corrente.getNome());

            TextView Ind = (TextView)oggetto.findViewById(R.id.Txt_Indirizzo);
            Ind.setText(nome_corrente.getIndirizzo());

            return oggetto;
        }
    }
    }

